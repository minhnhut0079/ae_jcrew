<?php

/* 
 * ここはアレだよ。
 * 何らかの理由によりデータが取れなかった案件を復活させるバッチです。
 * 
 */

require_once 'vendor/autoload.php';
require_once 'database.php';
require_once 'Scrape.php';
require_once 'CSVExporter.php';
require_once 'MstStartUrls.php';
require_once 'TblSubCategoryUrls.php';
require_once 'Utility.php';
require_once 'TblExportProductDatas.php';
require_once 'ProductData.php';

////初期化処理
date_default_timezone_set('Asia/Saigon');

//Loggerクラスの初期化
Logger::configure(dirname(__FILE__).'/vendor/apache/log4php/config.xml'); 
$logger = Logger::getLogger('logAppender');
$utility = new Utility();
$productData = new ProductData($logger);

$logger->info("リカバリバッチ開始");


//DB接続
$database = new Database($logger);
$pdo = $database->DB_connect('localhost','fermart_anthro', 'root', 'root');

$tblExportProductDatas = new TblExportProductDatas();
$productUlrs = $tblExportProductDatas->getGarbage($pdo);

$logger->info("取得件数" . count($productUlrs) . "件です。");


//各商品データにアクセス
foreach ($productUlrs as $productUrl) {
    $logger->info("商品ページにアクセス開始");

    $productEntity = $productData->scrape($productUrl['pcurl']);
    $productEntity->id = $productUrl['id'];
    $tblExportProductDatas->update($productEntity, $pdo);
    $logger->info("商品ページにアクセス終了");
    

}

$logger->info("[終了]商品詳細ページ取得処理");


