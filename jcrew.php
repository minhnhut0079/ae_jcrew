<?php

require_once 'phpQuery-onefile.php';
require_once 'vendor/autoload.php';
require_once 'database.php';
require_once 'Utility.php';
require_once 'ProductData.php';
require_once 'TblExportProductDatas.php';
require_once 'TblProductUrl.php';
require_once 'ProductJsonParser.php';
require_once 'ProductEntity.php';

// STEP1.起点URLから中間カテゴリを取得
$utility = new Utility();

$baseUrl ='https://www.ae.com/web/browse/category.jsp?catId=cat6640003';

$html = $utility->getHtml($baseUrl);

//var_dump($html);

$productListPattern = '@var category_json = (.*?)};@';

preg_match($productListPattern,$html,$matches);

//var_dump($matches[1]);

$rawJson = $matches[1]."}";

$return =json_decode($rawJson);

var_dump($return);
die();

$doc = phpQuery::newDocument($html);

$leftNav = $doc["div.leftnav"];
$patter = '@href="(.*?)".*?>(.*?)</@i';

preg_match_all($patter, $leftNav, $matches);
$cnt = count($matches[0]);
for ($i = 0; $i < $cnt; $i++) {
    echo $matches[1][$i] . "<br>";
    echo $matches[2][$i] . "<br>";
}


//// STEP2.中間カテゴリから詳細カテゴリを取得
//$middleCategoryURL = "https://www.jcrew.com/womens_feature/NewArrivals.jsp";
////$html = $utility->getHtml($middleCategoryURL);
//
//$doc = phpQuery::newDocument($html);
//
//$detailNav = $doc["ul.leftnav_sub_sub"];
//
//preg_match_all($patter, $detailNav, $matches);
//
//for ($i = 0; $i < $cnt; $i++) {
////    echo $matches[1][$i]."<br>";
////    echo $matches[2][$i]."<br>";
//}
//
//// STEP3.子カテゴリから商品一覧を取得
//$childCategoryURL = "https://www.jcrew.com/womens_category/sweaters.jsp";
//
////$html = $utility->getHtml($childCategoryURL);
//
//$doc = phpQuery::newDocument($html);
//
//$productNav = $doc["a.product-image-wrap"];
//
//foreach ($productNav as $value) {
////    echo pq($value)->attr('href').PHP_EOL;
//}
//
//// STEP4.商品一覧から商品詳細を取得
//$productURL = "https://www.jcrew.com/jp/mens_feature/BlueAndWhiteLook4_email/PRDOVR~17554/17554.jsp";
//
//$html = $utility->getHtml($productURL);
//
//$doc = phpQuery::newDocument($html);
//
//// タイトル
//
//$replace = array('®', '℠', '™');
//$title =  $doc["h1.notranslate"]->text();
//
//$fix_title = str_replace($replace, "", $title);
//
//echo $title;
//echo "<br>";
//
//echo "FIX TITEL".$fix_title;
//echo "<br>";
//
////Description
//echo $doc["div#prodDtlBody > p.notranslate"]->text();
//
//$lis = $doc["div#prodDtlBody > ul.tech-details > li"];
//
//foreach ($lis as $li) {
//    echo pq($li)->text() . "<br>";
//}
//
//$lis = $doc["div#sizefitDtlBody > ul > li"];
//
//foreach ($lis as $li) {
//    echo pq($li)->text() . "<br>";
//}
//
////画像URLを取得する。
//foreach ($doc->find('img.notranslate_alt') as $img){
//    $src = $img->getAttribute('src');
//    echo $src,'<br>';
//}
//
//// APIをコールする
//$baseAPIURL = "https://www.jcrew.com/browse2/ajax/product_details_ajax.jsp?sRequestedURL={URL}&prodCode={CODE}";
//
//$url = urlencode($productURL);
//
//preg_match("@.*/(.*?).jsp@i", $productURL, $matches);
//$productCode = $matches[1];
//$apiURL = str_replace("{URL}", $url, $baseAPIURL);
//$apiURL = str_replace("{CODE}", $productCode, $apiURL);
//
//$html = $utility->getHtml($apiURL);
//
////petitサイズがあるかチェックする。
//$html = "<HTML><HEAD></HEAD><BODY>" . $html . "</BODY></HTML>";
//
//$doc = phpQuery::newDocument($html);
//
//$diffUrlList = $doc['input:not(:checked)'];
//
//preg_match_all('@data-varianturl="(.*?)"@i', $diffUrlList, $matches);
//
//// 別サイズのURL
//foreach ($matches[1] as $value) {
//    echo $value . "<br>";
//}
//
////通常価格・セールの場合の金額を取る
//$data = explode("\n", $html);
//$data = preg_replace('/(\s|　)/', '', $data);
//$cnt = count($data);
//
//$price = "";
//$saleprice = "";
//
//for ($i = 0; $i < $cnt; $i++) {
//
//    if (strpos($data[$i], "full-price") !== false) {
//        echo $data[$i];
//        $price = $data[$i];
//    }
//
//    if (strpos($data[$i], "product-detail-price") !== false) {
//        $saleprice = $data[$i + 1];
//    }
//
//    if (strpos($data[$i], "data-color") !== false) {
//        if (!empty($saleprice)) {
//            preg_match_all('@data-color="(.*?)"@i', $data[$i], $matches);
//            echo $saleprice . "<br>";
//            echo $matches[1][0] . "<BR>";
//        }
//    }
//}
//
//$html = str_replace("\r\n", '', $html);
//$html = preg_replace('/(\s|　)/', '', $html);
//
//
////JSON部分だけ抜き出し
//preg_match_all("@.*?productDetailsJSON='(.*?)'@s", $html, $matches);
//
//$productArray = json_decode($matches[1][0], true);
//
//$colorSets = $productArray["colorset"];
//
//foreach ($colorSets as $color) {
//    echo $color["colordisplayname"] . "<BR>";
//    foreach ($color["sizes"] as $value) {
//        echo $value["sizelabel"] . "--->" . $value["inventory"] . "<BR>";
//    }
//}

