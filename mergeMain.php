<?php

/*
 * ここはアレだよ。
 * 何らかの理由によりデータが取れなかった案件を復活させるバッチです。
 * 
 */

require_once 'vendor/autoload.php';
require_once 'database.php';
require_once 'TblExportProductDatas.php';
require_once 'DuplicateUtility.php';

////初期化処理
date_default_timezone_set('Asia/Saigon');

//Loggerクラスの初期化
Logger::configure(dirname(__FILE__) . '/vendor/apache/log4php/config.xml');
$logger = Logger::getLogger('logAppender');
$duplicateUtility = new DuplicateUtility();
$logger->info("マージバッチ開始");

//DB接続
$database = new Database($logger);

$logger->info("********************");
$msg = '実行環境:' . Utility::getEnv();
$logger->info($msg);
$logger->info("********************");

$pdo = $database->DB_connect();

$tblExportProductDatas = new TblExportProductDatas();

//重複案件取得
$logger->info("STEP1:整形商品コードで重複削除開始");
$duplicateUrls = $tblExportProductDatas->getDuplicate($pdo);
$logger->info("取得件数" . count($duplicateUrls) . "件です。");

$logger->info("STEP3:重複削除開始");
foreach ($duplicateUrls as $duplicateUrl) {
    $logger->info("商品コード----->" . $duplicateUrl['productcode']);
    $sameProductUrls = $tblExportProductDatas->getUrls($pdo, $duplicateUrl['productcode']);

    $logger->info("重複取得件数" . count($sameProductUrls) . "件です。");

    $deleteUrls = $duplicateUtility->getDeleteUrls($sameProductUrls);

    $logger->info("削除取得件数" . count($deleteUrls) . "件です。");

    foreach ($deleteUrls as $deleteUrl) {
        $logger->info($deleteUrl['id'] . "--" . $deleteUrl['pcurl']);
    }

    //重複処理
    foreach ($deleteUrls as $deleteUrl) {
        $logger->info("削除対象ID---->" . $deleteUrl['id']);
        $tblExportProductDatas->delete($pdo, $deleteUrl['id']);
    }
    $logger->info('--------------------------------');
}
$logger->info("STEP3:整形商品コードで重複削除終了");

$logger->info("マージバッチ終了");

