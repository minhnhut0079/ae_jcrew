<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TblSubCategoryUrls
 *
 * @author user
 */
class TblSubCategoryUrls {

    public function get($pdo, $parentId, $subcategoryName, $subcategoryUrl) {

        $sql = "SELECT * FROM tbl_sub_category_urls WHERE parent_category_id = ? AND sub_category_name=? AND sub_category_url=?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $parentId, PDO::PARAM_INT);
        $stmt->bindValue(2, $subcategoryName, PDO::PARAM_STR);
        $stmt->bindValue(3, $subcategoryUrl, PDO::PARAM_STR);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_BOTH);

        if (empty($result)) {
            return "NONE";
        }

        return $result;
    }

    public function insert($pdo, $parentUrlID,$parentUrl,$key, $value) {
        $stmt = $pdo->prepare("INSERT INTO `tbl_sub_category_urls` (`id`, `parent_url`, `parent_category_id`, `sub_category_name`, `sub_category_url`, `created_at`) VALUES (NULL, ?, ?, ?, ?,now())");
        $stmt->bindParam(1, $parentUrl, PDO::PARAM_STR);
        $stmt->bindParam(2, $parentUrlID, PDO::PARAM_INT);
        $stmt->bindParam(3, $key, PDO::PARAM_STR);
        $stmt->bindParam(4, $value, PDO::PARAM_STR);
        $stmt->execute();
    }

    public function getAll($pdo) {
        $sql = "SELECT";
        $sql .= " ts.id as id, ";
        $sql .= " ts.sub_category_name as sub_category_name, ";
        $sql .= " ts.sub_category_url as sub_category_url, ";
        $sql .= " ms.top_category_name as top_category_name, ";
        $sql .= " ms.starting_name as starting_name ";
        $sql .= "FROM ";
        $sql .= " tbl_sub_category_urls as ts ";
        $sql .= " ,mst_start_urls as ms ";
        $sql .= "WHERE ";
        $sql .= " ts.parent_category_id = ms.id";
        $sql .= " AND";
        $sql .= " ts.status = 'TODO'";
        
        
//        echo $sql;
//        $sql = 'SELECT id,sub_category_name,sub_category_url FROM tbl_sub_category_urls';

        $stmt = $pdo->query($sql);

        $result = array();

        foreach ($stmt as $row) {
            $result[] = $row;
        }

        return $result;
    }

    public function update($pdo, $id, $status) {
        $sql = 'UPDATE tbl_sub_category_urls SET status = :status WHERE id = :id';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(':status', $status, PDO::PARAM_STR);
        $stmt->bindValue(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
    }

    public function truncate($pdo) {
        $sql = 'TRUNCATE TABLE tbl_sub_category_urls';
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
    }

}
