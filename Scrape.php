<?php

require_once 'Utility.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Scrape
 *
 * @author user
 */
class Scrape {


    public function getSubCategoryUrls($url) {

        $utility = new Utility();
        $html = $utility->getHtml($url);

        $subcategory = $this->extractSubcategory($html);

        return $subcategory;
    }

    public function extractSubcategory($html) {

        $text = str_replace(array("\r", "\n"), '', $html);

        $REGEX_SUB_CATEGORY = '@<a class="leftnav-link leftnav-l2-link.*href="(.*)".*->(.*)</@i';
        preg_match($REGEX_SUB_CATEGORY, $text, $matches);

        $date = strip_tags($matches[0], '<a>');

        $REGEX_SUB_CATEGORY2 = '@<a.*?class="leftnav-link.*?ng-href="(.*?)".*?>(.*?)</a>@';
        preg_match_all($REGEX_SUB_CATEGORY2, $date, $tess);
        
        $subcategoryPair = array_combine($tess[2], $tess[1]); 
        
        return $subcategoryPair;
    }


}
