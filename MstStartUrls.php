<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of StartUlrs
 *
 * @author user
 */
class MstStartUrls {

    public function getStartUrls($pdo) {
        $sql = 'SELECT id,top_category_name,starting_name,starting_url FROM mst_start_urls WHERE status="TODO"';

        $stmt = $pdo->query($sql);
                
        $result = array();

        foreach ($stmt as $row) {
            $result[] = $row;
        }

        return $result;
        
    }

    public function update($pdo, $id, $status) {
        $sql = 'UPDATE mst_start_urls SET status = :status WHERE id = :id';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(':status', $status, PDO::PARAM_STR);
        $stmt->bindValue(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
    }

    public function setTodo($pdo, $status) {
        $sql = 'UPDATE mst_start_urls SET status = :status';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(':status', $status, PDO::PARAM_STR);
        $stmt->execute();
    }
}
