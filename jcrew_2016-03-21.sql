-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 25, 2016 at 11:24 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jcrew_2016-03-21`
--

-- --------------------------------------------------------

--
-- Table structure for table `mst_start_urls`
--

CREATE TABLE `mst_start_urls` (
  `id` int(11) UNSIGNED NOT NULL,
  `top_category_name` varchar(255) DEFAULT NULL,
  `status` varchar(11) NOT NULL DEFAULT 'TODO',
  `starting_name` varchar(255) DEFAULT NULL,
  `starting_url` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mst_start_urls`
--

INSERT INTO `mst_start_urls` (`id`, `top_category_name`, `status`, `starting_name`, `starting_url`, `created_at`) VALUES
(1, 'clothing', 'TODO', 'Womens', 'http://www.ae.com/web/index.jsp?cid=AE_OMA_130712_0', '2016-03-24 06:31:50');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category_urls`
--

CREATE TABLE `tbl_category_urls` (
  `id` int(11) UNSIGNED NOT NULL,
  `parent_category` varchar(255) DEFAULT NULL,
  `category1` varchar(255) DEFAULT NULL,
  `category2` varchar(255) NOT NULL DEFAULT '',
  `category3` varchar(255) NOT NULL DEFAULT '',
  `status` varchar(255) NOT NULL DEFAULT 'TODO',
  `category_url` varchar(255) NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_category_urls`
--

INSERT INTO `tbl_category_urls` (`id`, `parent_category`, `category1`, `category2`, `category3`, `status`, `category_url`, `created_at`) VALUES
(1, 'women', 'Tops', '', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat10049&amp;navdetail=mega:women:c2:p1', '2016-03-24 04:55:07'),
(2, 'women', 'Bottoms', '', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat10051&amp;navdetail=mega:women:c3:p1', '2016-03-24 04:55:07'),
(3, 'women', 'Dresses + Rompers', '', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat1320034&amp;navdetail=mega:women:c4:p1', '2016-03-24 04:55:07'),
(4, 'women', 'Don''t Ask Why', '', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6570005&amp;navdetail=mega:women:c4:p2', '2016-03-24 04:55:07'),
(5, 'women', 'Socks', '', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7670006&amp;navdetail=mega:women:c4:p3', '2016-03-24 04:55:07'),
(6, 'women', 'Shoes', '', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat4840020&amp;navdetail=mega:women:c4:p4', '2016-03-24 04:55:07'),
(7, 'women', 'Accessories', '', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat4840018&amp;navdetail=mega:women:c4:p5', '2016-03-24 04:55:07'),
(8, 'women', 'Fragrance', '', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat5450026&amp;navdetail=mega:women:c4:p6', '2016-03-24 04:55:07'),
(9, 'women', '<span>Aerie', '', '', 'DONE', 'http://www.ae.com/aerie/index.jsp?navdetail=mega:women:c5:p7&amp;catId=cat4840006', '2016-03-24 04:55:07'),
(10, 'men', 'Tops', '', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat10025&amp;navdetail=mega:men:c2:p1', '2016-03-24 04:55:07'),
(11, 'men', 'Bottoms', '', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat10027&amp;navdetail=mega:men:c3:p1', '2016-03-24 04:55:07'),
(12, 'men', 'Underwear', '', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat10032&amp;navdetail=mega:men:c4:p1', '2016-03-24 04:55:07'),
(13, 'men', 'Socks', '', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat10066&amp;navdetail=mega:men:c4:p2', '2016-03-24 04:55:07'),
(14, 'men', 'Shoes', '', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat4840024&amp;navdetail=mega:men:c4:p3', '2016-03-24 04:55:07'),
(15, 'men', 'Accessories', '', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat4840022&amp;navdetail=mega:men:c4:p4', '2016-03-24 04:55:07'),
(16, 'men', 'Cologne', '', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat90024&amp;navdetail=mega:men:c4:p5', '2016-03-24 04:55:07'),
(17, 'women', 'Tops', 'Tops', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat10049', '2016-03-24 04:55:43'),
(18, 'women', 'Tops', 'Shirts', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat90038', '2016-03-24 04:55:43'),
(19, 'women', 'Tops', 'Tees', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat90030', '2016-03-24 04:55:43'),
(20, 'women', 'Tops', 'Graphic Tees', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat90042', '2016-03-24 04:55:43'),
(21, 'women', 'Tops', 'Tank Tops', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat380157', '2016-03-24 04:55:43'),
(22, 'women', 'Tops', 'Bralettes', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7530021', '2016-03-24 04:55:43'),
(23, 'women', 'Tops', 'Sweaters', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat1410002', '2016-03-24 04:55:43'),
(24, 'women', 'Tops', 'Hoodies + Sweatshirts', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat90048', '2016-03-24 04:55:43'),
(25, 'women', 'Tops', 'Jackets + Outerwear', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat4260032', '2016-03-24 04:55:43'),
(26, 'women', 'Bottoms', 'Bottoms', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat10051', '2016-03-24 04:55:57'),
(27, 'women', 'Bottoms', 'Shorts', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat380159', '2016-03-24 04:55:57'),
(28, 'women', 'Bottoms', 'Jeans', '', 'DONE', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat6430042', '2016-03-24 04:55:58'),
(29, 'women', 'Bottoms', 'Crops', '', 'DONE', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat7300012', '2016-03-24 04:55:58'),
(30, 'women', 'Bottoms', 'Pants', '', 'DONE', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat90034', '2016-03-24 04:55:58'),
(31, 'women', 'Bottoms', 'Leggings + Yoga', '', 'DONE', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat5610027', '2016-03-24 04:55:58'),
(32, 'women', 'Bottoms', 'Joggers + Soft Pants', '', 'DONE', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat7010091', '2016-03-24 04:55:58'),
(33, 'women', 'Bottoms', 'Skirts', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat5920105', '2016-03-24 04:55:58'),
(34, 'women', 'Dresses + Rompers', 'Dresses + Rompers', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat1320034', '2016-03-24 04:56:02'),
(35, 'women', 'Dresses + Rompers', 'Dress Trends', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7780013', '2016-03-24 04:56:02'),
(36, 'women', 'Dresses + Rompers', 'Shift Dresses', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6520112', '2016-03-24 04:56:03'),
(37, 'women', 'Dresses + Rompers', 'Fit & Flare Dresses', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6190040', '2016-03-24 04:56:03'),
(38, 'women', 'Dresses + Rompers', 'Rompers + Jumpsuits', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6270219', '2016-03-24 04:56:03'),
(39, 'women', 'Dresses + Rompers', 'Babydoll Dresses', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7160009', '2016-03-24 04:56:03'),
(40, 'women', 'Dresses + Rompers', 'Midi + Maxi Dresses', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat5450036', '2016-03-24 04:56:03'),
(41, 'women', 'Dresses + Rompers', 'Shirt Dresses', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat5180052', '2016-03-24 04:56:03'),
(42, 'women', 'Dresses + Rompers', 'Don''t Ask Why', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6920031', '2016-03-24 04:56:03'),
(43, 'women', 'Dresses + Rompers', 'Aerie Dresses & Rompers', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7540029', '2016-03-24 04:56:03'),
(44, 'women', 'Don''t Ask Why', 'Don''t Ask Why', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6570005', '2016-03-24 04:56:06'),
(45, 'women', 'Don''t Ask Why', 'New Arrivals', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7560028', '2016-03-24 04:56:06'),
(46, 'women', 'Don''t Ask Why', 'Tops', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7000014', '2016-03-24 04:56:06'),
(47, 'women', 'Don''t Ask Why', 'Dresses', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7000015', '2016-03-24 04:56:07'),
(48, 'women', 'Don''t Ask Why', 'Bottoms', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7000016', '2016-03-24 04:56:07'),
(49, 'women', 'Don''t Ask Why', 'Accessories', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7220027', '2016-03-24 04:56:07'),
(50, 'women', 'Socks', 'Socks', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7670006', '2016-03-24 04:56:08'),
(51, 'women', 'Socks', 'Crew Socks', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7670011', '2016-03-24 04:56:08'),
(52, 'women', 'Socks', 'Ankle Socks', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7670010', '2016-03-24 04:56:08'),
(53, 'women', 'Socks', 'No Show Socks', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7670008', '2016-03-24 04:56:08'),
(54, 'women', 'Socks', 'Boot Socks', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7670013', '2016-03-24 04:56:08'),
(55, 'women', 'Shoes', 'Shoes', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat4840020', '2016-03-24 04:56:17'),
(56, 'women', 'Shoes', 'Sandals + Flip Flops', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat3120012', '2016-03-24 04:56:17'),
(57, 'women', 'Shoes', 'Heels + Wedges', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat1440005', '2016-03-24 04:56:17'),
(58, 'women', 'Shoes', 'Flats', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat380151', '2016-03-24 04:56:17'),
(59, 'women', 'Shoes', 'Sneakers', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat380155', '2016-03-24 04:56:17'),
(60, 'women', 'Shoes', 'Boots', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat120147', '2016-03-24 04:56:17'),
(61, 'women', 'Shoes', 'Slippers + Mocs', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat5400010', '2016-03-24 04:56:17'),
(62, 'women', 'Shoes', 'Brands We Love', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7540002', '2016-03-24 04:56:17'),
(63, 'women', 'Accessories', 'Accessories', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat4840018', '2016-03-24 04:56:30'),
(64, 'women', 'Accessories', 'Jewelry', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat90050', '2016-03-24 04:56:30'),
(65, 'women', 'Accessories', 'Hair Accessories', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7670030', '2016-03-24 04:56:30'),
(66, 'women', 'Accessories', 'Sunglasses', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat1070002', '2016-03-24 04:56:30'),
(67, 'women', 'Accessories', 'Watches', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7730131', '2016-03-24 04:56:30'),
(68, 'women', 'Accessories', 'Scarves + Ponchos', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7190003', '2016-03-24 04:56:30'),
(69, 'women', 'Accessories', 'Hats', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat5400020', '2016-03-24 04:56:30'),
(70, 'women', 'Accessories', 'Socks', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6180024', '2016-03-24 04:56:30'),
(71, 'women', 'Accessories', 'Handbags + Backpacks', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat90052', '2016-03-24 04:56:30'),
(72, 'women', 'Accessories', 'Belts', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat1070004', '2016-03-24 04:56:30'),
(73, 'women', 'Accessories', 'Gifts', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7120038', '2016-03-24 04:56:31'),
(74, 'women', 'Accessories', 'Aerie Accessories', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7310003', '2016-03-24 04:56:31'),
(75, 'women', 'Fragrance', 'Fragrance', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat5450026', '2016-03-24 04:56:35'),
(76, 'women', 'Fragrance', 'Surf', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7340041', '2016-03-24 04:56:35'),
(77, 'women', 'Fragrance', 'Live Your Life', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7071173', '2016-03-24 04:56:35'),
(78, 'women', 'Fragrance', 'Me', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6180028', '2016-03-24 04:56:35'),
(79, 'women', 'Fragrance', 'Crush', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat5180084', '2016-03-24 04:56:35'),
(80, 'women', 'Fragrance', 'Live', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat5180086', '2016-03-24 04:56:35'),
(81, 'women', 'Fragrance', 'Bohemian', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat4320008', '2016-03-24 04:56:35'),
(82, 'women', 'Fragrance', 'Me Blush', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7550035', '2016-03-24 04:56:36'),
(83, 'women', 'Fragrance', 'Gift Sets', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6790018', '2016-03-24 04:56:36'),
(84, 'women', 'Fragrance', 'Vintage', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7010138', '2016-03-24 04:56:36'),
(85, 'women', 'Fragrance', 'Hidden Love', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7140023', '2016-03-24 04:56:36'),
(86, 'women', 'Fragrance', 'Cozy Up', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7140024', '2016-03-24 04:56:36'),
(87, 'women', 'Fragrance', 'Day Dream', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7140025', '2016-03-24 04:56:36'),
(88, 'men', 'Tops', 'Tops', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat10025', '2016-03-24 04:56:44'),
(89, 'men', 'Tops', 'Shirts', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat40005', '2016-03-24 04:56:44'),
(90, 'men', 'Tops', 'Tees', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat90012', '2016-03-24 04:56:44'),
(91, 'men', 'Tops', 'Graphic Tees', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat90018', '2016-03-24 04:56:44'),
(92, 'men', 'Tops', 'Polos', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat3130041', '2016-03-24 04:56:44'),
(93, 'men', 'Tops', 'Hoodies + Sweaters', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat90020', '2016-03-24 04:56:44'),
(94, 'men', 'Tops', 'Outerwear', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat380145', '2016-03-24 04:56:44'),
(95, 'men', 'Bottoms', 'Bottoms', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat10027', '2016-03-24 04:56:53'),
(96, 'men', 'Bottoms', 'Shorts', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat5180435', '2016-03-24 04:56:53'),
(97, 'men', 'Bottoms', 'Jeans', '', 'DONE', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat6430041', '2016-03-24 04:56:53'),
(98, 'men', 'Bottoms', 'Swim', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7250013', '2016-03-24 04:56:53'),
(99, 'men', 'Bottoms', 'Pants ', '', 'DONE', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat40003', '2016-03-24 04:56:53'),
(100, 'men', 'Bottoms', 'Joggers', '', 'DONE', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat7010052', '2016-03-24 04:56:53'),
(101, 'men', 'Underwear', 'Underwear', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat10032', '2016-03-24 04:57:08'),
(102, 'men', 'Underwear', 'Flex Athletic Underwear', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7640030', '2016-03-24 04:57:08'),
(103, 'men', 'Underwear', 'Trunks', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat4230029', '2016-03-24 04:57:08'),
(104, 'men', 'Underwear', 'Classic Briefs', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat4230031', '2016-03-24 04:57:08'),
(105, 'men', 'Underwear', 'Boxers', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat10074', '2016-03-24 04:57:08'),
(106, 'men', 'Socks', 'Socks', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat10066', '2016-03-24 04:57:13'),
(107, 'men', 'Socks', 'Fun Socks', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6960204', '2016-03-24 04:57:13'),
(108, 'men', 'Socks', 'Crew Socks', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7150002', '2016-03-24 04:57:13'),
(109, 'men', 'Socks', 'No Show Socks', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7730244', '2016-03-24 04:57:13'),
(110, 'men', 'Socks', 'Low Cut Socks', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6960205', '2016-03-24 04:57:13'),
(111, 'men', 'Shoes', 'Shoes', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat4840024', '2016-03-24 04:57:22'),
(112, 'men', 'Shoes', 'Flip Flops + Sandals', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat4520054', '2016-03-24 04:57:22'),
(113, 'men', 'Shoes', 'Sneakers', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat3130054', '2016-03-24 04:57:22'),
(114, 'men', 'Shoes', 'Casual Shoes', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat4870032', '2016-03-24 04:57:22'),
(115, 'men', 'Shoes', 'Boots', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat2770116', '2016-03-24 04:57:22'),
(116, 'men', 'Shoes', 'Slippers', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6080081', '2016-03-24 04:57:23'),
(117, 'men', 'Shoes', 'Brands We Love', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7540019', '2016-03-24 04:57:23'),
(118, 'men', 'Accessories', 'Accessories', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat4840022', '2016-03-24 04:57:33'),
(119, 'men', 'Accessories', 'Sunglasses', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat3450008', '2016-03-24 04:57:33'),
(120, 'men', 'Accessories', 'Hats', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat2380019', '2016-03-24 04:57:33'),
(121, 'men', 'Accessories', 'Bags + Backpacks', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6940280', '2016-03-24 04:57:33'),
(122, 'men', 'Accessories', 'Jewelry + Watches', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat90028', '2016-03-24 04:57:33'),
(123, 'men', 'Accessories', 'Belts', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat2970004', '2016-03-24 04:57:33'),
(124, 'men', 'Accessories', 'Wallets', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7230007', '2016-03-24 04:57:33'),
(125, 'men', 'Accessories', 'Gifts', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7130002', '2016-03-24 04:57:33'),
(126, 'men', 'Cologne', 'Cologne', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat90024', '2016-03-24 04:57:40'),
(127, 'men', 'Cologne', 'Surf', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7340042', '2016-03-24 04:57:40'),
(128, 'men', 'Cologne', 'Heritage', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7560137', '2016-03-24 04:57:40'),
(129, 'men', 'Cologne', 'Gift Sets', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6790017', '2016-03-24 04:57:40'),
(130, 'men', 'Cologne', 'Live Your Life', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7071226', '2016-03-24 04:57:40'),
(131, 'men', 'Cologne', '1977', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6490006', '2016-03-24 04:57:40'),
(132, 'men', 'Cologne', 'Real', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat4250079', '2016-03-24 04:57:40'),
(133, 'men', 'Cologne', 'Live', '', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat4250077', '2016-03-24 04:57:40'),
(134, 'women', 'Tops', 'Shirts', 'Blouses', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6050046', '2016-03-24 05:04:48'),
(135, 'women', 'Tops', 'Shirts', 'Sleeveless Shirts', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6050048', '2016-03-24 05:04:48'),
(136, 'women', 'Tops', 'Shirts', 'Button Down Shirts', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6050047', '2016-03-24 05:04:48'),
(137, 'women', 'Tops', 'Shirts', 'Flannels + Plaids', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7670003', '2016-03-24 05:04:48'),
(138, 'women', 'Tops', 'Shirts', 'Don''t Ask Why', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6920028', '2016-03-24 05:04:48'),
(139, 'women', 'Tops', 'Shirts', 'Aerie Shirts', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7560019', '2016-03-24 05:04:48'),
(140, 'women', 'Tops', 'Tees', 'Short Sleeve Tees', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7160021', '2016-03-24 05:06:56'),
(141, 'women', 'Tops', 'Tees', 'Long Sleeve Tees', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6640003', '2016-03-24 05:06:56'),
(142, 'women', 'Tops', 'Tees', 'Bodysuits', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6080084', '2016-03-24 05:06:56'),
(143, 'women', 'Tops', 'Tees', 'Polos', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6950009', '2016-03-24 05:06:56'),
(144, 'women', 'Tops', 'Tees', 'Don''t Ask Why', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6920024', '2016-03-24 05:06:56'),
(145, 'women', 'Tops', 'Tees', 'Aerie Tees', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7300004', '2016-03-24 05:06:56'),
(146, 'women', 'Tops', 'Graphic Tees', 'Short Sleeve Graphics', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6180011', '2016-03-24 05:07:22'),
(147, 'women', 'Tops', 'Graphic Tees', 'Graphic Tank Tops', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6270217', '2016-03-24 05:07:22'),
(148, 'women', 'Tops', 'Graphic Tees', 'Don''t Ask Why', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6920026', '2016-03-24 05:07:22'),
(149, 'women', 'Tops', 'Tank Tops', 'First Essentials', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7830001', '2016-03-24 05:07:58'),
(150, 'women', 'Tops', 'Tank Tops', 'Soft & Sexy Tanks', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6280046', '2016-03-24 05:07:59'),
(151, 'women', 'Tops', 'Tank Tops', 'Fashion Tanks', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6270035', '2016-03-24 05:07:59'),
(152, 'women', 'Tops', 'Tank Tops', 'Layering Tanks', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6270036', '2016-03-24 05:07:59'),
(153, 'women', 'Tops', 'Tank Tops', 'Graphic Tank Tops', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7300014', '2016-03-24 05:07:59'),
(154, 'women', 'Tops', 'Tank Tops', 'Don''t Ask Why', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6920027', '2016-03-24 05:07:59'),
(155, 'women', 'Tops', 'Tank Tops', 'Aerie Tank Tops', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7300005', '2016-03-24 05:07:59'),
(156, 'women', 'Tops', 'Bralettes', 'AEO Bralettes', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7830093', '2016-03-24 05:08:25'),
(157, 'women', 'Tops', 'Bralettes', 'Aerie Bralettes', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7530022', '2016-03-24 05:08:25'),
(158, 'women', 'Tops', 'Bralettes', 'Aerie Sports Bras', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7530023', '2016-03-24 05:08:25'),
(159, 'women', 'Tops', 'Sweaters', 'Pullover Sweaters', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6470515', '2016-03-24 05:09:00'),
(160, 'women', 'Tops', 'Sweaters', 'Cardigans', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6060003', '2016-03-24 05:09:00'),
(161, 'women', 'Tops', 'Sweaters', 'Sweater Tanks', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6470517', '2016-03-24 05:09:00'),
(162, 'women', 'Tops', 'Sweaters', 'Don''t Ask Why', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6920029', '2016-03-24 05:09:00'),
(163, 'women', 'Tops', 'Sweaters', 'Aerie Sweaters', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7140009', '2016-03-24 05:09:00'),
(164, 'women', 'Tops', 'Hoodies + Sweatshirts', 'Crew Sweatshirts', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6470512', '2016-03-24 05:09:22'),
(165, 'women', 'Tops', 'Hoodies + Sweatshirts', 'Pullover Sweatshirts', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6470514', '2016-03-24 05:09:22'),
(166, 'women', 'Tops', 'Hoodies + Sweatshirts', 'Zip Up Sweatshirts', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6470513', '2016-03-24 05:09:22'),
(167, 'women', 'Tops', 'Hoodies + Sweatshirts', 'Don''t Ask Why', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6920030', '2016-03-24 05:09:23'),
(168, 'women', 'Tops', 'Jackets + Outerwear', 'Denim Jackets + Vests', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7600012', '2016-03-24 05:11:36'),
(169, 'women', 'Tops', 'Jackets + Outerwear', 'Fashion Jackets + Vests', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7600013', '2016-03-24 05:11:36'),
(170, 'women', 'Bottoms', 'Shorts', 'Hi-Rise Shortie', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6960049', '2016-03-24 05:11:50'),
(171, 'women', 'Bottoms', 'Shorts', 'Hi-Rise Festival Shortie', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6280066', '2016-03-24 05:11:50'),
(172, 'women', 'Bottoms', 'Shorts', 'Super Hi-Rise Shortie', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7780006', '2016-03-24 05:11:50'),
(173, 'women', 'Bottoms', 'Shorts', 'Shortie', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6270221', '2016-03-24 05:11:50'),
(174, 'women', 'Bottoms', 'Shorts', 'Twill X Midi Short', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat6470526', '2016-03-24 05:11:50'),
(175, 'women', 'Bottoms', 'Shorts', 'Midi Short', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat3270002', '2016-03-24 05:11:50'),
(176, 'women', 'Bottoms', 'Shorts', 'Tomgirl Short', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7780003', '2016-03-24 05:11:50'),
(177, 'women', 'Bottoms', 'Shorts', 'Boy Midi Short', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7780004', '2016-03-24 05:11:50'),
(178, 'women', 'Bottoms', 'Shorts', 'Slouchy Bermuda Short', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7780005', '2016-03-24 05:11:50'),
(179, 'women', 'Bottoms', 'Shorts', 'Skinny Bermuda Short', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat3150141', '2016-03-24 05:11:50'),
(180, 'women', 'Bottoms', 'Shorts', 'Soft Short', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7330015', '2016-03-24 05:11:50'),
(181, 'women', 'Bottoms', 'Shorts', 'Aerie Shorts + Boxers', 'DONE', 'http://www.ae.com/web/browse/category.jsp?catId=cat7240055', '2016-03-24 05:11:50'),
(182, 'women', 'Bottoms', 'Shorts', 'Denim X Shorts', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7770018', '2016-03-24 05:11:50'),
(183, 'women', 'Bottoms', 'Shorts', 'Shorts Trends', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7720164', '2016-03-24 05:11:51'),
(184, 'women', 'Bottoms', 'Jeans', 'Jegging', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat20116', '2016-03-24 05:11:52'),
(185, 'women', 'Bottoms', 'Jeans', 'Hi-Rise Jegging', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat7010083', '2016-03-24 05:11:53'),
(186, 'women', 'Bottoms', 'Jeans', 'Super Hi-Rise Jegging', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat7720327', '2016-03-24 05:11:53'),
(187, 'women', 'Bottoms', 'Jeans', 'Jegging Crop', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat7240078', '2016-03-24 05:11:53'),
(188, 'women', 'Bottoms', 'Jeans', 'Hi-Rise Jegging Crop', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat7240077', '2016-03-24 05:11:53'),
(189, 'women', 'Bottoms', 'Jeans', 'Jegging Ankle', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat7220024', '2016-03-24 05:11:53'),
(190, 'women', 'Bottoms', 'Jeans', 'Skinny', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat1990002', '2016-03-24 05:11:53'),
(191, 'women', 'Bottoms', 'Jeans', 'Hi-Rise Skinny', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat7010084', '2016-03-24 05:11:53'),
(192, 'women', 'Bottoms', 'Jeans', 'Tomgirl', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat7240079', '2016-03-24 05:11:53'),
(193, 'women', 'Bottoms', 'Jeans', 'Straight', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat20108', '2016-03-24 05:11:53'),
(194, 'women', 'Bottoms', 'Jeans', 'Skinny Kick', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat5710043', '2016-03-24 05:11:53'),
(195, 'women', 'Bottoms', 'Jeans', 'Kick Boot', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat20110', '2016-03-24 05:11:53'),
(196, 'women', 'Bottoms', 'Jeans', 'Kick Crop', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat7720326', '2016-03-24 05:11:53'),
(197, 'women', 'Bottoms', 'Jeans', 'Boho Artist Flare', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat7290006', '2016-03-24 05:11:53'),
(198, 'women', 'Bottoms', 'Jeans', 'Hi-Rise Boho Artist Flare', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat7510067', '2016-03-24 05:11:53'),
(199, 'women', 'Bottoms', 'Jeans', 'Artist Flare', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat20112', '2016-03-24 05:11:53'),
(200, 'women', 'Bottoms', 'Jeans', 'Artist Crop', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat7290005', '2016-03-24 05:11:53'),
(201, 'women', 'Bottoms', 'Jeans', 'Festival Crop', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat7720325', '2016-03-24 05:11:53'),
(202, 'women', 'Bottoms', 'Jeans', 'A-Line', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat6460086', '2016-03-24 05:11:53'),
(203, 'women', 'Bottoms', 'Jeans', 'Overalls', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat6470013', '2016-03-24 05:11:53'),
(204, 'women', 'Bottoms', 'Jeans', 'The AEO Denim X Collection', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat7630010', '2016-03-24 05:11:53'),
(205, 'women', 'Bottoms', 'Jeans', 'Denim Trends', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7720164', '2016-03-24 05:11:53'),
(206, 'women', 'Bottoms', 'Crops', 'Jegging Crop', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat7770019', '2016-03-24 05:11:55'),
(207, 'women', 'Bottoms', 'Crops', 'Hi-Rise Jegging Crop', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat7770022', '2016-03-24 05:11:55'),
(208, 'women', 'Bottoms', 'Crops', 'Jegging Ankle', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat7770024', '2016-03-24 05:11:55'),
(209, 'women', 'Bottoms', 'Crops', 'Artist Crop', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat7770025', '2016-03-24 05:11:55'),
(210, 'women', 'Bottoms', 'Crops', 'Kick Crop', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat7770026', '2016-03-24 05:11:55'),
(211, 'women', 'Bottoms', 'Crops', 'Festival Crop', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat7770028', '2016-03-24 05:11:55'),
(212, 'women', 'Bottoms', 'Pants', 'Jegging Pants ', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat5180042', '2016-03-24 05:11:57'),
(213, 'women', 'Bottoms', 'Pants', 'Tomgirl Pants', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat7550007', '2016-03-24 05:11:57'),
(214, 'women', 'Bottoms', 'Pants', 'Skinny Pants', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat6470529', '2016-03-24 05:11:57'),
(215, 'women', 'Bottoms', 'Pants', 'Kick Boot Pants', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat6470528', '2016-03-24 05:11:57'),
(216, 'women', 'Bottoms', 'Pants', 'Boho Artist Flare Pants', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat6470530', '2016-03-24 05:11:57'),
(217, 'women', 'Bottoms', 'Pants', 'Utility Jogger Pants', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat6710002', '2016-03-24 05:11:57'),
(218, 'women', 'Bottoms', 'Pants', 'Jegging Crops', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat5180044', '2016-03-24 05:11:57'),
(219, 'women', 'Bottoms', 'Leggings + Yoga', 'Leggings', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat7060019', '2016-03-24 05:11:59'),
(220, 'women', 'Bottoms', 'Leggings + Yoga', 'Shorts', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat7720218', '2016-03-24 05:11:59'),
(221, 'women', 'Bottoms', 'Leggings + Yoga', 'Aerie Leggings + Yoga', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat7140005', '2016-03-24 05:11:59'),
(222, 'women', 'Bottoms', 'Joggers + Soft Pants', 'AEO Joggers + Soft Pants', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat7140016', '2016-03-24 05:12:02'),
(223, 'women', 'Bottoms', 'Joggers + Soft Pants', 'Don''t Ask Why Joggers + Soft Pants', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat7580005', '2016-03-24 05:12:02'),
(224, 'women', 'Bottoms', 'Joggers + Soft Pants', 'Aerie Joggers + Lounge', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7140004', '2016-03-24 05:12:02'),
(225, 'women', 'Bottoms', 'Skirts', 'Mini Skirts', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7720338', '2016-03-24 05:12:05'),
(226, 'women', 'Bottoms', 'Skirts', 'Maxi Skirts', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7720339', '2016-03-24 05:12:05'),
(227, 'women', 'Bottoms', 'Skirts', 'Soft Shorts', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7720340', '2016-03-24 05:12:05'),
(228, 'women', 'Dresses + Rompers', 'Dress Trends', 'White Here, White Now', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7780017', '2016-03-24 05:12:11'),
(229, 'women', 'Dresses + Rompers', 'Dress Trends', 'The Bold Shoulder', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7780014', '2016-03-24 05:12:11'),
(230, 'women', 'Dresses + Rompers', 'Dress Trends', 'Prints Like Wildflowers', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7780016', '2016-03-24 05:12:11'),
(231, 'women', 'Dresses + Rompers', 'Dress Trends', 'High Times', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7780015', '2016-03-24 05:12:12'),
(232, 'women', 'Dresses + Rompers', 'Dress Trends', 'Back to Back Detail', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7790003', '2016-03-24 05:12:12'),
(233, 'women', 'Dresses + Rompers', 'Dress Trends', 'Effortless Is More', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7780019', '2016-03-24 05:12:12'),
(234, 'women', 'Don''t Ask Why', 'New Arrivals', 'Street Style', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7830067', '2016-03-24 05:12:36'),
(235, 'women', 'Don''t Ask Why', 'New Arrivals', 'Weekend Vibes', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7830068', '2016-03-24 05:12:36'),
(236, 'women', 'Don''t Ask Why', 'New Arrivals', 'Festival Season', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7830071', '2016-03-24 05:12:36'),
(237, 'women', 'Don''t Ask Why', 'New Arrivals', 'Girls'' Night Out', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7830069', '2016-03-24 05:12:36'),
(238, 'women', 'Shoes', 'Sandals + Flip Flops', 'Flip Flops', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat6950125', '2016-03-24 05:13:11'),
(239, 'women', 'Shoes', 'Sandals + Flip Flops', 'Sandals', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7010139', '2016-03-24 05:13:11'),
(240, 'women', 'Shoes', 'Boots', 'Ankle Boots', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat6470546', '2016-03-24 05:13:29'),
(241, 'women', 'Shoes', 'Boots', 'Mid Calf Boots', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat6470547', '2016-03-24 05:13:29'),
(242, 'women', 'Shoes', 'Boots', 'Knee High Boots', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat6470611', '2016-03-24 05:13:29'),
(243, 'women', 'Shoes', 'Brands We Love', 'BC', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7540014', '2016-03-24 05:13:37'),
(244, 'women', 'Shoes', 'Brands We Love', 'Bed Stu', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7540018', '2016-03-24 05:13:37'),
(245, 'women', 'Shoes', 'Brands We Love', 'Birkenstock', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7540005', '2016-03-24 05:13:37'),
(246, 'women', 'Shoes', 'Brands We Love', 'Dr. Scholl', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7770023', '2016-03-24 05:13:37'),
(247, 'women', 'Shoes', 'Brands We Love', 'Dolce Vita', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7540009', '2016-03-24 05:13:37'),
(248, 'women', 'Shoes', 'Brands We Love', 'Eastland', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7540047', '2016-03-24 05:13:37'),
(249, 'women', 'Shoes', 'Brands We Love', 'Hunter', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7540013', '2016-03-24 05:13:37'),
(250, 'women', 'Shoes', 'Brands We Love', 'K.Swiss', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7590014', '2016-03-24 05:13:37'),
(251, 'women', 'Shoes', 'Brands We Love', 'Keds', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7540011', '2016-03-24 05:13:37'),
(252, 'women', 'Shoes', 'Brands We Love', 'Matisse', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7540017', '2016-03-24 05:13:37'),
(253, 'women', 'Shoes', 'Brands We Love', 'Minnetonka', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7540015', '2016-03-24 05:13:37'),
(254, 'women', 'Shoes', 'Brands We Love', 'New Balance', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7540012', '2016-03-24 05:13:37'),
(255, 'women', 'Shoes', 'Brands We Love', 'Pajar', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7640006', '2016-03-24 05:13:37'),
(256, 'women', 'Shoes', 'Brands We Love', 'Palladium', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7540010', '2016-03-24 05:13:37'),
(257, 'women', 'Shoes', 'Brands We Love', 'Pony', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7730282', '2016-03-24 05:13:38'),
(258, 'women', 'Shoes', 'Brands We Love', 'Reebok', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7590013', '2016-03-24 05:13:38'),
(259, 'women', 'Shoes', 'Brands We Love', 'Swedish Hasbeens', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7540007', '2016-03-24 05:13:38'),
(260, 'women', 'Shoes', 'Brands We Love', 'Teva', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7540006', '2016-03-24 05:13:38'),
(261, 'women', 'Accessories', 'Jewelry', 'Necklaces', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7120043', '2016-03-24 05:13:44'),
(262, 'women', 'Accessories', 'Jewelry', 'Rings', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7120044', '2016-03-24 05:13:44'),
(263, 'women', 'Accessories', 'Jewelry', 'Bracelets', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7120040', '2016-03-24 05:13:45'),
(264, 'women', 'Accessories', 'Jewelry', 'Earrings', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7120041', '2016-03-24 05:13:45'),
(265, 'women', 'Accessories', 'Jewelry', 'Temporary Tattoos ', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7340040', '2016-03-24 05:13:45'),
(266, 'women', 'Accessories', 'Jewelry', 'Body Jewelry', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7830094', '2016-03-24 05:13:45'),
(267, 'women', 'Accessories', 'Sunglasses', 'Aviator Sunglasses', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7720411', '2016-03-24 05:13:49'),
(268, 'women', 'Accessories', 'Sunglasses', 'Round Sunglasses', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7720410', '2016-03-24 05:13:49'),
(269, 'women', 'Accessories', 'Sunglasses', 'Cat Eye Sunglasses', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7720409', '2016-03-24 05:13:49'),
(270, 'women', 'Accessories', 'Sunglasses', 'Statement Sunglasses', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7720408', '2016-03-24 05:13:49'),
(271, 'women', 'Accessories', 'Sunglasses', 'Classic Sunglasses', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7720412', '2016-03-24 05:13:50'),
(272, 'women', 'Accessories', 'Socks', 'Crews', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7150145', '2016-03-24 05:13:58'),
(273, 'women', 'Accessories', 'Socks', 'Ankle Socks', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7240081', '2016-03-24 05:13:59'),
(274, 'women', 'Accessories', 'Socks', 'no-shows', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7150146', '2016-03-24 05:13:59'),
(275, 'women', 'Accessories', 'Socks', 'Boot Socks', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7150144', '2016-03-24 05:13:59'),
(276, 'women', 'Accessories', 'Gifts', 'Pool Party', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7720343', '2016-03-24 05:14:07'),
(277, 'women', 'Accessories', 'Gifts', 'Housewares', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7630018', '2016-03-24 05:14:07'),
(278, 'women', 'Accessories', 'Gifts', 'Beauty Essentials', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7630019', '2016-03-24 05:14:07'),
(279, 'women', 'Accessories', 'Gifts', 'Write On', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7630020', '2016-03-24 05:14:08'),
(280, 'women', 'Accessories', 'Gifts', 'Signature Scents', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7670063', '2016-03-24 05:14:08'),
(281, 'men', 'Tops', 'Shirts', 'Short Sleeve Shirts', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat6000004', '2016-03-24 05:14:51'),
(282, 'men', 'Tops', 'Shirts', 'Plaid & Patterned Shirts', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat6300043', '2016-03-24 05:14:51'),
(283, 'men', 'Tops', 'Shirts', 'Denim Shirts', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat6390037', '2016-03-24 05:14:51'),
(284, 'men', 'Tops', 'Shirts', 'Solid Shirts', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat6000003', '2016-03-24 05:14:51'),
(285, 'men', 'Tops', 'Shirts', 'Slim Fit Shirts', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7640010', '2016-03-24 05:14:51'),
(286, 'men', 'Tops', 'Tees', 'Crew Tees', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat6470437', '2016-03-24 05:14:55'),
(287, 'men', 'Tops', 'Tees', 'V-Neck Tees', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat6470439', '2016-03-24 05:14:55'),
(288, 'men', 'Tops', 'Tees', 'Henley Tees', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat6470438', '2016-03-24 05:14:55'),
(289, 'men', 'Tops', 'Tees', 'Slim T-Shirts', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat6300023', '2016-03-24 05:14:55'),
(290, 'men', 'Tops', 'Tees', 'Athletic Tees', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7730152', '2016-03-24 05:14:55'),
(291, 'men', 'Tops', 'Tees', 'Fashion Tees', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat1320095', '2016-03-24 05:14:55'),
(292, 'men', 'Tops', 'Tees', 'Tank Tops', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat6270186', '2016-03-24 05:14:55'),
(293, 'men', 'Tops', 'Tees', 'Hooded Tees', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7560128', '2016-03-24 05:14:55'),
(294, 'men', 'Tops', 'Graphic Tees', 'Trend Graphic Tees', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat4470006', '2016-03-24 05:15:00'),
(295, 'men', 'Tops', 'Graphic Tees', 'Elevated Graphic Tees', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat6230003', '2016-03-24 05:15:00'),
(296, 'men', 'Tops', 'Graphic Tees', 'Vintage Graphic Tees', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7520181', '2016-03-24 05:15:00'),
(297, 'men', 'Tops', 'Graphic Tees', 'Logo Graphic Tees', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat6230004', '2016-03-24 05:15:00'),
(298, 'men', 'Tops', 'Graphic Tees', 'Graphic Tanks', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7120047', '2016-03-24 05:15:00'),
(299, 'men', 'Tops', 'Graphic Tees', 'Hooded Graphic Tees', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7670039', '2016-03-24 05:15:00'),
(300, 'men', 'Tops', 'Graphic Tees', 'Long Sleeve Graphic Tees', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat6470440', '2016-03-24 05:15:00'),
(301, 'men', 'Tops', 'Polos', 'Pique Polos', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat6890058', '2016-03-24 05:15:04'),
(302, 'men', 'Tops', 'Polos', 'Jersey Polos', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat6910255', '2016-03-24 05:15:04'),
(303, 'men', 'Tops', 'Polos', 'Slim Polos', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7140022', '2016-03-24 05:15:04'),
(304, 'men', 'Tops', 'Hoodies + Sweaters', 'Pullover Hoodies', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat6050050', '2016-03-24 05:15:07'),
(305, 'men', 'Tops', 'Hoodies + Sweaters', 'Zip Up Hoodies', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat6050051', '2016-03-24 05:15:07'),
(306, 'men', 'Tops', 'Hoodies + Sweaters', 'Crew Sweatshirts', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat6470442', '2016-03-24 05:15:07'),
(307, 'men', 'Tops', 'Hoodies + Sweaters', 'Lightweight Hoodies', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat6050049', '2016-03-24 05:15:07'),
(308, 'men', 'Tops', 'Hoodies + Sweaters', 'Sweaters', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7140014', '2016-03-24 05:15:07'),
(309, 'men', 'Bottoms', 'Shorts', 'Slim Flat Front Shorts', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7730136', '2016-03-24 05:15:20'),
(310, 'men', 'Bottoms', 'Shorts', 'Classic Flat Front Shorts', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7220033', '2016-03-24 05:15:20'),
(311, 'men', 'Bottoms', 'Shorts', 'Classic Cargo Shorts', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7220032', '2016-03-24 05:15:20'),
(312, 'men', 'Bottoms', 'Shorts', 'Baja Shorts', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7730147', '2016-03-24 05:15:20'),
(313, 'men', 'Bottoms', 'Shorts', 'Hybrid Shorts', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7730148', '2016-03-24 05:15:20'),
(314, 'men', 'Bottoms', 'Shorts', 'Denim Shorts', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7260044', '2016-03-24 05:15:20'),
(315, 'men', 'Bottoms', 'Shorts', 'Longer Length Flat Front Shorts', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7730149', '2016-03-24 05:15:20'),
(316, 'men', 'Bottoms', 'Shorts', 'Longer Length Cargo Shorts', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7730150', '2016-03-24 05:15:20'),
(317, 'men', 'Bottoms', 'Shorts', 'Prep Shorts', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7730151', '2016-03-24 05:15:20'),
(318, 'men', 'Bottoms', 'Shorts', 'Fleece Shorts', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7760031', '2016-03-24 05:15:20'),
(319, 'men', 'Bottoms', 'Shorts', 'Swim Trunks', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7220042', '2016-03-24 05:15:20'),
(320, 'men', 'Bottoms', 'Shorts', 'AEO Flex / Shorts', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7730243', '2016-03-24 05:15:20'),
(321, 'men', 'Bottoms', 'Jeans', 'Slim Straight', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat5180058', '2016-03-24 05:15:22'),
(322, 'men', 'Bottoms', 'Jeans', 'Slim', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat6390033', '2016-03-24 05:15:22'),
(323, 'men', 'Bottoms', 'Jeans', 'Skinny', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat5850028', '2016-03-24 05:15:22'),
(324, 'men', 'Bottoms', 'Jeans', 'Super Skinny', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat7100003', '2016-03-24 05:15:22'),
(325, 'men', 'Bottoms', 'Jeans', 'Original Straight', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat4670040', '2016-03-24 05:15:22'),
(326, 'men', 'Bottoms', 'Jeans', 'Relaxed Straight', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat20072', '2016-03-24 05:15:22'),
(327, 'men', 'Bottoms', 'Jeans', 'Original Boot', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat20068', '2016-03-24 05:15:22'),
(328, 'men', 'Bottoms', 'Jeans', 'Classic Bootcut', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat20066', '2016-03-24 05:15:22'),
(329, 'men', 'Bottoms', 'Jeans', 'Loose', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat7010002', '2016-03-24 05:15:22'),
(330, 'men', 'Bottoms', 'Jeans', 'The AEO FLEX Denim Collection', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat7630012', '2016-03-24 05:15:22'),
(331, 'men', 'Bottoms', 'Jeans', 'Denim Trends', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7720180', '2016-03-24 05:15:22'),
(332, 'men', 'Bottoms', 'Pants ', 'Skinny', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat6150144', '2016-03-24 05:15:27'),
(333, 'men', 'Bottoms', 'Pants ', 'Slim', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat7270003', '2016-03-24 05:15:27'),
(334, 'men', 'Bottoms', 'Pants ', 'Slim Straight', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat5930002', '2016-03-24 05:15:27'),
(335, 'men', 'Bottoms', 'Pants ', 'Original Straight', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat5930003', '2016-03-24 05:15:27'),
(336, 'men', 'Bottoms', 'Pants ', 'Relaxed Straight', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat5930004', '2016-03-24 05:15:27'),
(337, 'men', 'Bottoms', 'Pants ', 'Original Boot', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat6880046', '2016-03-24 05:15:27'),
(338, 'men', 'Bottoms', 'Pants ', 'Joggers', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7560138', '2016-03-24 05:15:27'),
(339, 'men', 'Bottoms', 'Pants ', 'Super Skinny', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat7510117', '2016-03-24 05:15:27'),
(340, 'men', 'Bottoms', 'Pants ', 'The AEO Flex Collection', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat7630013', '2016-03-24 05:15:27'),
(341, 'men', 'Bottoms', 'Joggers', 'Twill Joggers', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat7630074', '2016-03-24 05:15:29'),
(342, 'men', 'Bottoms', 'Joggers', 'Fleece Joggers', 'TODO', 'http://www.ae.com/web/browse/category_products.jsp?catId=cat7240087', '2016-03-24 05:15:29'),
(343, 'men', 'Underwear', 'Flex Athletic Underwear', '9" Flex Trunk', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7520172', '2016-03-24 05:15:44');
INSERT INTO `tbl_category_urls` (`id`, `parent_category`, `category1`, `category2`, `category3`, `status`, `category_url`, `created_at`) VALUES
(344, 'men', 'Underwear', 'Flex Athletic Underwear', '6" Flex Trunk', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat4230010', '2016-03-24 05:15:44'),
(345, 'men', 'Underwear', 'Flex Athletic Underwear', '3" Flex Trunk', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat6430154', '2016-03-24 05:15:44'),
(346, 'men', 'Underwear', 'Flex Athletic Underwear', 'Flex Boxer', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7670067', '2016-03-24 05:15:44'),
(347, 'men', 'Underwear', 'Flex Athletic Underwear', 'Flex / Base Layer', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7670066', '2016-03-24 05:15:44'),
(348, 'men', 'Underwear', 'Trunks', '6" Classic Trunk', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat4430018', '2016-03-24 05:15:49'),
(349, 'men', 'Underwear', 'Trunks', '3" Classic Trunk', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat4430016', '2016-03-24 05:15:49'),
(350, 'men', 'Underwear', 'Boxers', 'Boxer Shorts', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7520176', '2016-03-24 05:15:59'),
(351, 'men', 'Underwear', 'Boxers', 'Slim Knit Boxers', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7520175', '2016-03-24 05:15:59'),
(352, 'men', 'Underwear', 'Boxers', 'Flex Boxers', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7520174', '2016-03-24 05:15:59'),
(353, 'men', 'Shoes', 'Flip Flops + Sandals', 'Flip Flops', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7300002', '2016-03-24 05:16:25'),
(354, 'men', 'Shoes', 'Flip Flops + Sandals', 'Sandals', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7300003', '2016-03-24 05:16:25'),
(355, 'men', 'Shoes', 'Brands We Love', 'Birkenstock', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7540026', '2016-03-24 05:16:39'),
(356, 'men', 'Shoes', 'Brands We Love', 'CAT', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7630029', '2016-03-24 05:16:39'),
(357, 'men', 'Shoes', 'Brands We Love', 'Clarks', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7540025', '2016-03-24 05:16:39'),
(358, 'men', 'Shoes', 'Brands We Love', 'Eastland', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7540032', '2016-03-24 05:16:39'),
(359, 'men', 'Shoes', 'Brands We Love', 'Hunter', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7540024', '2016-03-24 05:16:39'),
(360, 'men', 'Shoes', 'Brands We Love', 'K.Swiss', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7560001', '2016-03-24 05:16:39'),
(361, 'men', 'Shoes', 'Brands We Love', 'Minnetonka', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7540023', '2016-03-24 05:16:39'),
(362, 'men', 'Shoes', 'Brands We Love', 'New Balance', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7540022', '2016-03-24 05:16:39'),
(363, 'men', 'Shoes', 'Brands We Love', 'Pajar', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7630097', '2016-03-24 05:16:39'),
(364, 'men', 'Shoes', 'Brands We Love', 'PF Flyers', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7540034', '2016-03-24 05:16:39'),
(365, 'men', 'Shoes', 'Brands We Love', 'Pony', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7720328', '2016-03-24 05:16:39'),
(366, 'men', 'Shoes', 'Brands We Love', 'Reebok', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7540033', '2016-03-24 05:16:39'),
(367, 'men', 'Shoes', 'Brands We Love', 'Teva', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7540021', '2016-03-24 05:16:39'),
(368, 'men', 'Shoes', 'Brands We Love', 'Thorogood', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7560020', '2016-03-24 05:16:39'),
(369, 'men', 'Shoes', 'Brands We Love', 'Timberland', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7540031', '2016-03-24 05:16:39'),
(370, 'men', 'Shoes', 'Brands We Love', 'Tretorn', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7540020', '2016-03-24 05:16:39'),
(371, 'men', 'Shoes', 'Brands We Love', 'Woolrich', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7590006', '2016-03-24 05:16:39'),
(372, 'men', 'Shoes', 'Brands We Love', 'Wolverine', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7610006', '2016-03-24 05:16:40'),
(373, 'men', 'Accessories', 'Gifts', 'Pool Party', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7720382', '2016-03-24 05:17:02'),
(374, 'men', 'Accessories', 'Gifts', 'Housewares', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7630026', '2016-03-24 05:17:02'),
(375, 'men', 'Accessories', 'Gifts', 'Signiture Scents', 'TODO', 'http://www.ae.com/web/browse/category.jsp?catId=cat7670062', '2016-03-24 05:17:02');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_export_product_datas`
--

CREATE TABLE `tbl_export_product_datas` (
  `id` int(11) UNSIGNED NOT NULL,
  `pcurl` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `name` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `product_code` varchar(50) NOT NULL,
  `brand` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `color` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `color_code` varchar(255) NOT NULL,
  `sex` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `category1` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `category2` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `category3` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `category4` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `category5` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `category6` varchar(255) NOT NULL,
  `category7` varchar(255) NOT NULL,
  `category8` varchar(255) NOT NULL,
  `price` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `salesprice` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `Product_Detail1` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `Product_Detail2` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `Product_Detail3` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `Product_Detail4` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `Product_Detail5` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `Product_Detail6` varchar(255) NOT NULL,
  `Product_Detail7` varchar(255) NOT NULL,
  `Product_Detail8` varchar(255) NOT NULL,
  `Product_Detail9` varchar(255) NOT NULL,
  `Product_Detail10` varchar(255) NOT NULL,
  `size1` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `size2` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `size3` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `size4` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `size5` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `size6` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `size7` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `size8` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `size9` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `size10` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `size11` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `size12` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `size13` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `size14` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `size15` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `size16` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `size17` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `size18` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `size19` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `size20` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `image1` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `image2` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `image3` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `image4` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `image5` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `image6` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `image7` varchar(1024) NOT NULL,
  `image8` varchar(1024) NOT NULL,
  `image9` varchar(1024) NOT NULL,
  `image10` varchar(1024) NOT NULL,
  `stock` varchar(15) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=sjis;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ignore_urls`
--

CREATE TABLE `tbl_ignore_urls` (
  `id` int(11) UNSIGNED NOT NULL,
  `url` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_urls`
--

CREATE TABLE `tbl_product_urls` (
  `id` int(11) UNSIGNED NOT NULL,
  `parent_category` varchar(255) DEFAULT NULL,
  `category1` varchar(255) DEFAULT NULL,
  `category2` varchar(255) NOT NULL DEFAULT '',
  `category3` varchar(255) NOT NULL DEFAULT '',
  `status` varchar(255) NOT NULL DEFAULT 'TODO',
  `product_url` varchar(255) NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_product_urls`
--

INSERT INTO `tbl_product_urls` (`id`, `parent_category`, `category1`, `category2`, `category3`, `status`, `product_url`, `created_at`) VALUES
(1, 'women', 'Tops', '', '', 'TODO', 'https://www.ae.com/web/browse/product_details.jsp?productId=1305_9840_073&catId=cat6180011', '2016-03-24 05:17:30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mst_start_urls`
--
ALTER TABLE `mst_start_urls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_category_urls`
--
ALTER TABLE `tbl_category_urls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_export_product_datas`
--
ALTER TABLE `tbl_export_product_datas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ignore_urls`
--
ALTER TABLE `tbl_ignore_urls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_product_urls`
--
ALTER TABLE `tbl_product_urls`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mst_start_urls`
--
ALTER TABLE `mst_start_urls`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_category_urls`
--
ALTER TABLE `tbl_category_urls`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=376;
--
-- AUTO_INCREMENT for table `tbl_export_product_datas`
--
ALTER TABLE `tbl_export_product_datas`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_ignore_urls`
--
ALTER TABLE `tbl_ignore_urls`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_product_urls`
--
ALTER TABLE `tbl_product_urls`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4249;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
