<?php

require_once 'phpQuery-onefile.php';
require_once 'vendor/autoload.php';
require_once 'Utility.php';
require_once 'TblCategoryUrl.php';
require_once 'TblProductUrl.php';


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SubCategoryScrape
 *
 * @author user
 */
class SubCategoryScrape {

    public $logger = "";

    function __construct() {
        $this->logger = Logger::getLogger('main');
    }

    public function scrapeProduct($pdo, $parentCategory, $categoryUrl, $category1, $category2,$category3) {

        $utility = new Utility();
        $categoryUrl = $categoryUrl . "?iNextCategory=-1";
        $html = $utility->getHtml($categoryUrl);

        $productURLs = $this->getProductUrl($html);
        
        $tblProductUrl = new TblProductUrl();
        
        foreach ($productURLs as $productURL) {
            $tblProductUrl->insert($pdo, $productURL, $parentCategory, $category1, $category2, $category3);
        }
        
    }

    public function scrapeMidlleCategoryUrl($pdo, $parentCategory, $categoryUrl, $category1) {

        //HTMLデータ取得
        $utility = new Utility();
        $html = $utility->getHtml($categoryUrl);

        //中間カテゴリURLの取得
        $productURLs = $this->getMiddleCategoryURL($html);

        //DBへ登録
        $tblCategoryUrl = new TblCategoryUrl();

        foreach ($productURLs as $categoryName => $categoryUrl) {
            foreach($categoryUrl as $key => $value){
                $categoryName = html_entity_decode($categoryName);
                $tblCategoryUrl->insert($pdo, $categoryName, $key, "","", $value);
            }
        }
    }

    public function scrapeDetailCategoryUrl($pdo, $parentCategory, $categoryUrl, $category1) {

        //HTMLデータ取得
        $utility = new Utility();
        $html = $utility->getHtml($categoryUrl);

        //商品URLの取得
        $detailURLs = $this->getDetailCategoryURL($html);

        //DBへ登録
        $tblCategoryUrl = new TblCategoryUrl();

        foreach ($detailURLs as $categoryName => $categoryUrl) {
            $categoryName = html_entity_decode(strip_tags($categoryName));
            $tblCategoryUrl->insert($pdo, $parentCategory, $category1, $categoryName, "", $categoryUrl);
        }
    }

    public function scrapeDetailSubCategoryUrl($pdo, $parentCategory,$category1,$category2, $categoryUrl) {
        //HTMLデータ取得
        $utility = new Utility();
        $html = $utility->getHtml($categoryUrl);
        //商品URLの取得
        $detailURLs = $this->getDetailSubCategoryURL($html);
        //DBへ登録
        $tblCategoryUrl = new TblCategoryUrl();

        foreach ($detailURLs as $categoryName => $categoryUrl) {
            $categoryName = html_entity_decode(strip_tags($categoryName));
            $tblCategoryUrl->insert($pdo, $parentCategory, $category1,$category2, $categoryName, $categoryUrl);
        }
    }

    private function checkPageNation($html) {
        $REGEX_SUB_CATEGORY = '@<a class="page-number-right enabled".*?href="(.*?)">@i';
        preg_match($REGEX_SUB_CATEGORY, $html, $pagignate);

        if (empty($pagignate[1])) {
            return "";
        }
        return $pagignate[1];
    }

    private function getMiddleCategoryURL($html) {

        $doc = phpQuery::newDocument($html);

        // get url woment
        $divwomen = $doc["div.women"];
        $st_wm = $divwomen['li strong'];
        $patter = '@href="(.*?)".*?>(.*?)</@i';
        preg_match_all($patter, $st_wm, $matches);
        $cnt = count($matches[0]);
        $result['women'] = array_combine($matches[2], $matches[1]);

        // get url woment
        $divmen = $doc["div.men"];
        $st_m = $divmen['li strong'];
        preg_match_all($patter,$st_m, $matches);
        $result['men'] = array_combine($matches[2], $matches[1]);


        return $result;
    }

    private function getDetailCategoryURL($html) {

        $doc = phpQuery::newDocument($html);

        $detailNav = $doc["div.catNav"];
        $patter = '@href="(.*?)".*?>(.*?)</@i';

        preg_match_all($patter, $detailNav, $matches);

        $result = array_combine($matches[2], $matches[1]);

        return $result;
    }

    private function getDetailSubCategoryURL($html) {

        $doc = phpQuery::newDocument($html);
        $detailNav = $doc["ul.menu"];
        $detailNav = $detailNav->find("ul");
        $patter = '@href="(.*?)".*?>(.*?)</@i';

        preg_match_all($patter, $detailNav, $matches);
        $result = array_combine($matches[2], $matches[1]);
        return $result;
    }

    public function getProductUrl($html) {

        $doc = phpQuery::newDocument($html);


        $productListPattern = '@var category_json = (.*?)};@';

        preg_match($productListPattern,$html,$matches);

        $rawJson = $matches[1]."}";

        $return =json_decode($rawJson);

        $i = 0;
        foreach($return->displayCategoryIds as $_displayCategoryIds){
            foreach($return->availableCategories->$_displayCategoryIds->availablePrdIds as $_products){
                $product_url[$i] = 'https://www.ae.com/web/browse/product_details.jsp?productId='.$_products->prdId.'&catId='.$_displayCategoryIds;
                $i++;
            }

        }
        $product_url = array_unique($product_url);
        return $product_url;


        /*$productNav = $doc["a.product-image-wrap"];

        $urlList = array();

        foreach ($productNav as $value) {
            $urlList[] = pq($value)->attr('href');
        }*/
        

    }

}
