<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TblExportProductDatas
 *
 * @author user
 */
class TblExportProductDatas {

    public function insert($productEntity, $pdo) {
        $sql = "INSERT INTO `tbl_export_product_datas`";
        $sql .= " (`pcurl`,"
                . " `name`, "
                . "`brand`, "
                . "`color`,"
                . "`colorID`,"
                . "`sex`, "
                . "`category1`,"
                . " `category2`, "
                . "`category3`, "
                . "`category4`,"
                . "`category5`,"
                . "`category6`,"
                . "`category7`,"
                . "`category8`,"
                . " `price`,"
                . " `salesprice`,"
                . " `Product_Detail1`, "
                . "`Product_Detail2`, "
                . "`Product_Detail3`, "
                . "`Product_Detail4`, "
                . "`Product_Detail5`, "
                . "`Product_Detail6`, "
                . "`Product_Detail7`, "
                . "`Product_Detail8`, "
                . "`Product_Detail9`, "
                . "`Product_Detail10`, "
                . "`size1`, "
                . "`size2`, "
                . "`size3`, "
                . "`size4`, "
                . "`size5`, "
                . "`size6`, "
                . "`size7`, "
                . "`size8`, "
                . "`size9`, "
                . "`size10`,"
                . " `size11`,"
                . " `size12`, "
                . "`size13`, "
                . "`size14`, "
                . "`size15`, "
                . "`size16`,"
                . " `size17`,"
                . " `size18`, "
                . "`size19`,"
                . " `size20`, "
                . " `image1`, "
                . "`image2`, "
                . "`image3`, "
                . "`image4`, "
                . "`image5`, "
                . "`image6`, "
                . "`image7`, "
                . "`image8`, "
                . "`image9`, "
                . "`image10`, "
                . "`stock`, "
                . "`created_at`) ";
        $sql .= "  VALUES ";
        $sql .= "( ?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "?, "
                . "now())";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1, $productEntity->pcurl, PDO::PARAM_STR);
        $stmt->bindParam(2, $productEntity->name, PDO::PARAM_STR);
        $stmt->bindParam(3, $productEntity->brand, PDO::PARAM_STR);
        $stmt->bindParam(4, $productEntity->color, PDO::PARAM_STR);
        $stmt->bindParam(5, $productEntity->colorID, PDO::PARAM_STR);
        $stmt->bindParam(6, $productEntity->sex, PDO::PARAM_STR);
        $stmt->bindParam(7, $productEntity->category1, PDO::PARAM_STR);
        $stmt->bindParam(8, $productEntity->category2 ? $productEntity->category2 : '', PDO::PARAM_STR);
        $stmt->bindParam(9, $productEntity->category3 ? $productEntity->category3 : '', PDO::PARAM_STR);
        $stmt->bindParam(10, $productEntity->category4 ? $productEntity->category4 : '', PDO::PARAM_STR);
        $stmt->bindParam(11, $productEntity->category5 ? $productEntity->category5 : '', PDO::PARAM_STR);
        $stmt->bindParam(12, $productEntity->price ? $productEntity->price : '', PDO::PARAM_STR);
        $stmt->bindParam(13, $productEntity->salesprice ? $productEntity->salesprice : '', PDO::PARAM_STR);
        $stmt->bindParam(14, $productEntity->Product_Detail1 ? $productEntity->Product_Detail1 : '', PDO::PARAM_STR);
        $stmt->bindParam(15, $productEntity->Product_Detail2 ? $productEntity->Product_Detail2 : '', PDO::PARAM_STR);
        $stmt->bindParam(16, $productEntity->Product_Detail3 ? $productEntity->Product_Detail3 : '', PDO::PARAM_STR);
        $stmt->bindParam(17, $productEntity->Product_Detail4 ? $productEntity->Product_Detail4 : '', PDO::PARAM_STR);
        $stmt->bindParam(18, $productEntity->Product_Detail5 ? $productEntity->Product_Detail5 : '', PDO::PARAM_STR);
        $stmt->bindParam(19, $productEntity->Product_Detail6 ? $productEntity->Product_Detail6 : '', PDO::PARAM_STR);
        $stmt->bindParam(20, $productEntity->Product_Detail7 ? $productEntity->Product_Detail7 : '', PDO::PARAM_STR);
        $stmt->bindParam(21, $productEntity->Product_Detail8 ? $productEntity->Product_Detail8 : '', PDO::PARAM_STR);

        $stmt->bindParam(22, $productEntity->size1 ? $productEntity->size1 : '', PDO::PARAM_STR);
        $stmt->bindParam(23, $productEntity->size2 ? $productEntity->size2 : '', PDO::PARAM_STR);
        $stmt->bindParam(24, $productEntity->size3 ? $productEntity->size3 : '', PDO::PARAM_STR);
        $stmt->bindParam(25, $productEntity->size4 ? $productEntity->size4 : '', PDO::PARAM_STR);
        $stmt->bindParam(26, $productEntity->size5 ? $productEntity->size5 : '', PDO::PARAM_STR);
        $stmt->bindParam(27, $productEntity->size6 ? $productEntity->size6 : '', PDO::PARAM_STR);
        $stmt->bindParam(28, $productEntity->size7 ? $productEntity->size7 : '', PDO::PARAM_STR);
        $stmt->bindParam(29, $productEntity->size8 ? $productEntity->size8 : '', PDO::PARAM_STR);
        $stmt->bindParam(30, $productEntity->size9 ? $productEntity->size9 : '', PDO::PARAM_STR);
        $stmt->bindParam(31, $productEntity->size10 ? $productEntity->size10 : '', PDO::PARAM_STR);
        $stmt->bindParam(32, $productEntity->size11 ? $productEntity->size11 : '', PDO::PARAM_STR);
        $stmt->bindParam(33, $productEntity->size12 ? $productEntity->size12 : '', PDO::PARAM_STR);
        $stmt->bindParam(34, $productEntity->size13 ? $productEntity->size13 : '', PDO::PARAM_STR);
        $stmt->bindParam(35, $productEntity->size14 ? $productEntity->size14 : '', PDO::PARAM_STR);
        $stmt->bindParam(36, $productEntity->size15 ? $productEntity->size15 : '', PDO::PARAM_STR);
        $stmt->bindParam(37, $productEntity->size16 ? $productEntity->size16 : '', PDO::PARAM_STR);
        $stmt->bindParam(38, $productEntity->size17 ? $productEntity->size17 : '', PDO::PARAM_STR);
        $stmt->bindParam(39, $productEntity->size18 ? $productEntity->size18 : '', PDO::PARAM_STR);
        $stmt->bindParam(40, $productEntity->size19 ? $productEntity->size19 : '', PDO::PARAM_STR);
        $stmt->bindParam(41, $productEntity->size20 ? $productEntity->size20 : '', PDO::PARAM_STR);

        $stmt->bindParam(42, $productEntity->image1 ? $productEntity->image1 : '', PDO::PARAM_STR);
        $stmt->bindParam(43, $productEntity->image2 ? $productEntity->image2 : '', PDO::PARAM_STR);
        $stmt->bindParam(44, $productEntity->image3 ? $productEntity->image3 : '', PDO::PARAM_STR);
        $stmt->bindParam(45, $productEntity->image4 ? $productEntity->image4 : '', PDO::PARAM_STR);
        $stmt->bindParam(46, $productEntity->image5 ? $productEntity->image5 : '', PDO::PARAM_STR);
        $stmt->bindParam(47, $productEntity->image6 ? $productEntity->image6 : '', PDO::PARAM_STR);
        $stmt->bindParam(48, $productEntity->image7 ? $productEntity->image7 : '', PDO::PARAM_STR);
        $stmt->bindParam(49, $productEntity->image8 ? $productEntity->image8 : '', PDO::PARAM_STR);
        $stmt->bindParam(50, $productEntity->image9 ? $productEntity->image9 : '', PDO::PARAM_STR);
        $stmt->bindParam(51, $productEntity->image10 ? $productEntity->image10 : '', PDO::PARAM_STR);
        $stmt->bindParam(52, $productEntity->stock ? $productEntity->stock : '', PDO::PARAM_STR);

        $stmt->execute();
    }

    public function getGarbage($pdo) {

        $sql = 'SELECT * FROM tbl_export_product_datas WHERE name = "" AND stock =""';

        $stmt = $pdo->query($sql);

        $result = array();

        foreach ($stmt as $row) {
            $result[] = $row;
        }

        return $result;
    }

    public function getDuplicate($pdo) {
        $sql = 'SELECT productcode,COUNT(id) as num FROM tbl_export_product_datas GROUP BY productcode HAVING COUNT(productcode) > 1';

        $stmt = $pdo->query($sql);

        $result = array();

        foreach ($stmt as $row) {
            $result[] = $row;
        }

        return $result;
    }

    public function getUrls($pdo, $productCode) {
        $sql = 'SELECT * FROM tbl_export_product_datas WHERE productcode = :productCode';
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(':productCode', $productCode, PDO::PARAM_STR);
        $stmt->execute();

        $result = array();

        foreach ($stmt as $row) {
            $result[] = $row;
        }

        return $result;
    }

    public function select($pdo, $id) {
        $sql = 'SELECT COUNT(*) FROM tbl_export_product_datas WHERE pcurl LIKE :id ';
        $stmt = $pdo->prepare($sql);
        $id = "%".$id."%";
        
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $result = array();

        foreach ($stmt as $row) {
            $result[] = $row;
        }

        return $result;
    }

    public function delete($pdo, $id) {
        $sql = 'DELETE FROM tbl_export_product_datas WHERE id = :id';
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
    }

    public function getOriginalProductCodeUrls($pdo) {
        $sql = 'SELECT id, productcode,CHAR_LENGTH(productcode)  FROM tbl_export_product_datas WHERE CHAR_LENGTH(productcode) > 18';

        $stmt = $pdo->query($sql);

        $result = array();

        foreach ($stmt as $row) {
            $result[] = $row;
        }

        return $result;
    }

    public function updateProductCode($pdo, $id, $newProductCode) {
        $sql = 'UPDATE tbl_export_product_datas SET productcode = :productcode WHERE id = :id';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(':productcode', $newProductCode, PDO::PARAM_STR);
        $stmt->bindValue(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
    }

    public function truncate($pdo) {
        $sql = 'TRUNCATE TABLE tbl_export_product_datas';
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
    }

}
