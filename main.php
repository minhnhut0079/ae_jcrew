﻿<?php

require_once 'vendor/autoload.php';
require_once 'database.php';
require_once 'Scrape.php';
require_once 'MstStartUrls.php';
require_once 'TblSubCategoryUrls.php';
require_once 'TblSubCategoryUrls.php';
require_once 'TblExportProductDatas.php';
require_once 'TblCategoryUrl.php';
require_once 'TblIgnoreUrl.php';
require_once 'Utility.php';
require_once 'SubCategoryScrape.php';
require_once 'ProductData.php';
require_once 'TblExportProductDatas.php';
require_once 'TblProductUrl.php';

////初期化処理
date_default_timezone_set('Asia/Saigon');
$utility = new Utility();

//Loggerクラスの初期化
Logger::configure(dirname(__FILE__) . '/vendor/apache/log4php/config.xml');
$logger = Logger::getLogger('logAppender');
$logger->info('JCREW商品コード取得バッチ開始');

//DB接続
$database = new Database($logger);

$logger->info("********************");
$msg = '実行環境:' . Utility::getEnv();
$logger->info($msg);
$logger->info("********************");

$pdo = $database->DB_connect();
//初期化
$mstStartUrls = new MstStartUrls();
$tblCategoryUrl = new TblCategoryUrl();
$tblProductUrl = new TblProductUrl();
$tblExportProductDatas = new TblExportProductDatas();

$logger->info('データ初期化開始');
$mstStartUrls->setTodo($pdo, "TODO");

$tblCategoryUrl->truncate($pdo);
$tblProductUrl->truncate($pdo);
$tblExportProductDatas->truncate($pdo);

////メインロジック
//カテゴリ変動チェック
//起点URL取得
$logger->info('対象起点URLの取得');

$urls = $mstStartUrls->getStartUrls($pdo);
$logger->info('対象URLは' . count($urls) . 'です');
$logger->info('対象起点URLの終了');

$subCategoryScrape = new SubCategoryScrape();

// STEP1.起点URLから中間カテゴリを取得
foreach ($urls as $url) {
    $logger->info('起点URLの商品データ取得開始');
    $subCategoryScrape->scrapeMidlleCategoryUrl($pdo, $url['top_category_name'], $url['starting_url'], "");
    $mstStartUrls->update($pdo, $url['id'], "DONE");
    $logger->info('起点URLの商品データ取得終了');
}
// STEP2.中間カテゴリから詳細カテゴリを取得

$middleCategoryURLs = $tblCategoryUrl->get($pdo);

foreach ($middleCategoryURLs as $middleCategoryURL) {
    $logger->info('詳細カテゴリチェック開始');
    $subCategoryScrape->scrapeDetailCategoryUrl($pdo, $middleCategoryURL['parent_category'], $middleCategoryURL['category_url'], $middleCategoryURL['category1']);

    $logger->info('詳細カテゴリチェック終了');
}

$middleCategoryURLs = array('0'=>array('category_url'=>'http://www.ae.com/web/browse/category.jsp?catId=cat7600013'));
$middleCategoryURL['parent_category'] = 1;
$middleCategoryURL['category1'] = 2;
$middleCategoryURL['category2'] = 3;

// STEP 3. Get SUB MENU CHILD

$middleCategoryURLs = $tblCategoryUrl->get($pdo);

foreach ($middleCategoryURLs as $middleCategoryURL) {
    $logger->info('詳細カテゴリチェック開始');
    $subCategoryScrape->scrapeDetailSubCategoryUrl($pdo, $middleCategoryURL['parent_category'],$middleCategoryURL['category1'],$middleCategoryURL['category2'], $middleCategoryURL['category_url']);
//    $subCategoryScrape->scrapeDetailSubCategoryUrl($pdo, 1,2,3, $middleCategoryURL['category_url']);

    $logger->info('詳細カテゴリチェック終了');
}

// STEP3.子カテゴリから商品一覧を取得
$allCategoryURLs = $tblCategoryUrl->get($pdo);

// STEP3.5.Reject Ignore URLS compareto TblIgnore and TblCategoryUrl
$tblIgnoreUrl = new TblIgnoreUrl();
$IgnoreUrls = $tblIgnoreUrl->get($pdo);

foreach ($allCategoryURLs as $allCategoryURL) {

    $logger->info('商品URL抽出開始');

    $logger->info($allCategoryURL['category_url']);

    foreach ($IgnoreUrls as $ignoreUrl) {
        $logger->info($ignoreUrl['url']);
        if (trim($ignoreUrl['url']) == trim($allCategoryURL['category_url'])) {
            $logger->info('除外リストにマッチ');
            $tblCategoryUrl->update($pdo, $allCategoryURL['id'], "IGNORE");
            continue 2;
        }
    }

    $subCategoryScrape->scrapeProduct($pdo, $allCategoryURL['parent_category'], $allCategoryURL['category_url'], $allCategoryURL['category1'], $allCategoryURL['category2'], $allCategoryURL['category3']);

    $tblCategoryUrl->update($pdo, $allCategoryURL['id'], "DONE");

    $logger->info('商品URL抽出終了');
}


$TblExportProductDatas = new TblExportProductDatas();
$ProductData = new ProductData();

$TblExportProductDatas->truncate($pdo);

$productUrls = $tblProductUrl->get($pdo,2);
foreach($productUrls as $productUrl){
    $product_details = $ProductData->getProductDetail($pdo,$productUrl['product_url'],$productUrl['parent_category']);
    $status = 'DONE';
    $TblProductUrl->update($pdo,$productUrl['id'], $status);
}

$logger->info('JCREW商品コード取得バッチ終了');
