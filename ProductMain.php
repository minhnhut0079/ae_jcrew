<?php

require_once 'vendor/autoload.php';
require_once 'database.php';
require_once 'Utility.php';
require_once 'ProductData.php';
require_once 'TblExportProductDatas.php';
require_once 'TblProductUrl.php';

/*
 * 商品情報を取得するメインクラスです。
 */

////初期化処理
//Loggerクラスの初期化
Logger::configure(dirname(__FILE__) . '/vendor/apache/log4php/config.xml');
$logger = Logger::getLogger('logAppender');

$logger->info("[開始]商品詳細ページ取得処理");

$utility = new Utility();
$productData = new ProductData($logger);

//DB接続
$database = new Database($logger);

$logger->info("********************");
$msg = '実行環境:' . Utility::getEnv();
$logger->info($msg);
$logger->info("********************");

$pdo = $database->DB_connect();
$tblExportProductDatas = new TblExportProductDatas();

//商品情報の取得
$tblProductUrl = new TblProductUrl();

//全件を親カテゴリ含めてちょっとずつ取る。（取得件数の可変は後ほど調整）
$limit = 200;
$productUlrs = $tblProductUrl->get($pdo, $limit);

$logger->info("取得件数" . count($productUlrs) . "件です。");

//並列化のため、ステータスでロック
foreach ($productUlrs as $productUrl) {
    $logger->info("ステータスロック開始");
    $tblProductUrl->update($pdo, $productUrl['id'], "DOING");
    $logger->info("ステータスロック終了");
}

//各商品データにアクセス
foreach ($productUlrs as $productUrl) {
    $logger->info("商品ページにアクセス開始");

    $url = $productUrl['product_url'];

    $productEntityList = $productData->scrape($url);

    foreach ($productEntityList as $productEntities) {
        foreach ($productEntities as $productEntity) {
            $productEntity->pcurl = $url;
            $productEntity->sex = $productUrl['parent_category'];
            $productEntity->category1 = $productUrl['category1'];
            $productEntity->category2 = $productUrl['category2'];
            $tblExportProductDatas->save($productEntity, $pdo);
        }
    }

    $tblProductUrl->update($pdo, $productUrl['id'], "DONE");
    $logger->info("商品ページにアクセス終了");
}

$logger->info("[終了]商品詳細ページ取得処理");
