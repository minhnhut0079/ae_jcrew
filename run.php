<?php

require_once 'vendor/autoload.php';

//Loggerクラスの初期化
$logger = Logger::getLogger('main');
date_default_timezone_set('Asia/Saigon');

$logger->info('処理開始' . date("Y年m月d日 H時i分s秒"));
$time_start = microtime(true);

foreach (range(1, 100) as $a)  {
        passthru('php ProductMain.php', $retun);
        sleep(3);
 }

$logger->info('処理終了' . date("Y年m月d日 H時i分s秒"));
$time_end = microtime(true);
$time = $time_end - $time_start;

$logger->info('処理にかかった時間' . ($time / 60) . "分");
passthru('sudo halt', $retun);
