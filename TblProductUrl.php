<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TblProductUrl
 *
 * @author user
 */
class TblProductUrl {

    public function insert($pdo, $productURL, $parentCategory, $category1, $category2, $category3) {


            $stmt = $pdo->prepare("INSERT INTO `tbl_product_urls` ( "
                    . "`parent_category`, "
                    . "`category1`, "
                    . "`category2`, "
                    . "`category3`, "
                    . "`product_url` ,"
                    . "`created_at`"
                    . ") VALUES ("
                    . "?, "
                    . "?, "
                    . "?, "
                    . "?, "
                    . "?, "
                    . "now() "
                    . ")");

            $stmt->bindParam(1, $parentCategory, PDO::PARAM_STR);
            $stmt->bindParam(2, $category1, PDO::PARAM_STR);
            $stmt->bindParam(3, $category2, PDO::PARAM_STR);
            $stmt->bindParam(4, $category3, PDO::PARAM_STR);
            $stmt->bindParam(5, $productURL, PDO::PARAM_STR);
            $stmt->execute();
    }

    public function get($pdo,$limit) {
        $sql = 'SELECT * FROM tbl_product_urls WHERE status="TODO" LIMIT '.$limit;

        $stmt = $pdo->query($sql);

        $result = array();

        foreach ($stmt as $row) {
            $result[] = $row;
        }

        return $result;
    }

    public function update($pdo, $id, $status) {
        $sql = 'UPDATE tbl_product_urls SET status = :status WHERE id = :id';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(':status', $status, PDO::PARAM_STR);
        $stmt->bindValue(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
    }

     public function truncate($pdo) {
        $sql = 'TRUNCATE TABLE tbl_product_urls';
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
    }

    //put your code here
}
