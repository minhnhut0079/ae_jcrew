<?php

require_once 'ProductEntity.php';
require_once 'Utility.php';


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductParser
 *
 * @author user
 */
class ProductParser {

    private $xpath;
    public $logger;

    function __construct() {
        $this->logger = Logger::getLogger('logAppender');
    }
    public function getImage($productUrl){
        $utility = new Utility();
        $html = $utility->getHtml($productUrl);
        $doc = phpQuery::newDocument($html);
        $body = $doc['div.carousel-thumbs'];
        preg_match_all('/(src)=("[^"]*")/i',$body, $src);
        $result = array();
        foreach($src[0] as $_src){
            $ex = explode('?',$_src);
            $result[] = str_replace('src="//pics.ae.com/is/image/aeo/','',$ex[0]);
        }
        return $result;
    }
    public function parse($html, $url) {

        $productEntities = array();
        $productEntity = new ProductEntity();
        $apiHtml = $this->getApiHTML($url);

        $doc = phpQuery::newDocument($html);

        $this->logger->info("商品HTMLパース開始");

        //製品名登録
        $replace = array('®', '℠', '™');
        $title = $doc["h1.notranslate"]->text();
        $fix_title = str_replace($replace, "", $title);

        $productEntity->name = $fix_title;

        //プロダクトIDを設定
        $productId = "jc-" . mb_strtolower($this->getProductId($url));
        $pCode = $this->getProductId($url);

        //Description登録
        $productEntity->description = $doc["div#prodDtlBody > p.notranslate"]->text();

        // Product Detail登録
        $deslis = $doc["div#prodDtlBody > ul.tech-details > li"];
        $productDetails = array();
        foreach ($deslis as $li) {
            $productDetails[] = pq($li)->text();
        }
        $productEntity->setProductDetail($productDetails);

        // Size Fit登録
        $sizelis = $doc["div#sizefitDtlBody > ul > li"];
        $sizeFitDetails = array();
        foreach ($sizelis as $li) {
            $sizeFitDetails[] = pq($li)->text();
        }
        $productEntity->setSizeFitDetail($sizeFitDetails);

        //価格を取得する
        $normalPrice = $this->getPrice($apiHtml);
        $productEntity->price = $normalPrice;

        $salePriceList = $this->getSalePrice($apiHtml);

        //カラー別サイズを取得して増殖させる。
        $productEntities = $this->getColorType($apiHtml, $productEntity, $productId, $pCode);

        //カラーとセール金額をマージ
        $productEntities = $this->mergeSalePriceAndColor($salePriceList, $productEntities);

        //画像URLを取得する。
        $imageTagArray = $this->getImageTag($doc);

        //カラーと画像イメージをマージ
        $productEntities = $this->mergeImageTagAndColor($imageTagArray, $productEntities);


        $this->logger->info("商品HTMLパース終了");

        return $productEntities;
    }

    public function getPrice($apiHtml) {

        $data = explode("\n", $apiHtml);

        $trimData = array_filter($data, function($data) {
            return trim($data);
        });

        $trimData = array_values($trimData);

        $cnt = count($trimData);
        $price = "";

        for ($i = 0; $i < $cnt; $i++) {
            if (strpos($trimData[$i], "full-price") !== false) {
                $price = trim(strip_tags($trimData[$i]));
                if (empty($price)) {
                    $price = trim(strip_tags($trimData[$i + 1]));
                }
            }
        }

        return trim($price);
    }

    public function getSalePrice($html) {

        $data = explode("\n", $html);
        $data = preg_replace('/(\s|　)/', '', $data);
        $cnt = count($data);

        $colorPriceList = array();
        $saleprice = "";

        for ($i = 0; $i < $cnt; $i++) {

            if (strpos($data[$i], "product-detail-price") !== false) {
                $saleprice = $data[$i + 1];
            }

            if (strpos($data[$i], "data-color") !== false) {
                if (!empty($saleprice)) {
                    preg_match_all('@data-color="(.*?)"@i', $data[$i], $matches);
                    $colorPriceList[$matches[1][0]] = $saleprice;
                }
            }
        }

        return $colorPriceList;
    }

    public function getColorType($apiHtml, $productEntity, $productId, $pCode) {

        $apiHtml = str_replace("\r\n", '', $apiHtml);
        $apiHtml = preg_replace('/(\s|　)/', '', $apiHtml);

        preg_match_all("@.*?productDetailsJSON='(.*?)'@s", $apiHtml, $matches);

        $productArray = json_decode($matches[1][0], true);
        $colorSets = $productArray["colorset"];

        foreach ($colorSets as $color) {
            $tmpproductEntity = clone $productEntity;
            $tmpproductEntity->productcode = $productId . "-" . mb_strtolower($color["color"]);
            $tmpproductEntity->pcode = $pCode;
            $tmpproductEntity->color = $color["colordisplayname"];
            $tmpproductEntity->colorCode = $color["color"];

            $sizeArray = array();
            foreach ($color["sizes"] as $value) {
                $sizeArray[] = $value["sizelabel"];
            }
            $tmpproductEntity->setSize($sizeArray);
            $productEntities[] = $tmpproductEntity;
        }

        return $productEntities;
    }

    public function getApiHTML($productURL) {

        // APIをコールする
        $baseAPIURL = "https://www.jcrew.com/browse2/ajax/product_details_ajax.jsp?sRequestedURL={URL}&prodCode={CODE}";

        $url = urlencode($productURL);

        preg_match("@.*/(.*?).jsp@i", $productURL, $matches);
        $productCode = $matches[1];
        $apiURL = str_replace("{URL}", $url, $baseAPIURL);
        $apiURL = str_replace("{CODE}", $productCode, $apiURL);

        $utility = new Utility();
        $apiHtml = $utility->getHtml($apiURL);

        return $apiHtml;
    }

    public function mergeSalePriceAndColor($salePriceList, $productEntities) {

        foreach ($productEntities as $productEntity) {

            if (array_key_exists($productEntity->colorCode, $salePriceList)) {
                $productEntity->saleprice = $salePriceList[$productEntity->colorCode];
            }
        }

        return $productEntities;
    }

    public function getProductId($url) {

        preg_match("@.*/(.*?).jsp@i", $url, $matches);
        $productCode = $matches[1];

        return $productCode;
    }

    public function getImageTag($doc) {
        //画像URLを取得する。
        $imageTagArray = array();
        $imgs = $doc->find('img.notranslate_alt');

        foreach ($imgs as $img) {
            $src = $img->getAttribute('src');

            preg_match_all("@.*jcrew/.*_.*_(.*)\?@i", $src, $matches);

            if (!empty($matches[1][0])) {
                $imageTag = $matches[1][0];
            }

            $imageTagArray[] = $imageTag;
        }
        $imageTagArray = array_filter($imageTagArray);
        $imageTagArray = array_unique($imageTagArray);

        return $imageTagArray;
    }

    public function mergeImageTagAndColor($imageTagArray, $productEntities) {

        $baseImageURL = 'https://i.s-jcrew.com/is/image/jcrew/{PRODUCTCODE}_{COLORCODE}_{IMAGETAG}?$pdp_enlarge$';


        foreach ($productEntities as $productEntity) {
            $productCode = strtoupper($productEntity->pcode);
            $colorCode = strtoupper($productEntity->colorCode);

            $apiURL = str_replace("{PRODUCTCODE}", $productCode, $baseImageURL);
            $apiURL = str_replace("{COLORCODE}", $colorCode, $apiURL);

            $imageUrl = str_replace("_{IMAGETAG}", "", $apiURL);

            $imageUrls = array();
            $imageUrls[] = $imageUrl;

            foreach ($imageTagArray as $imageTag) {

                $imageUrl = str_replace("{IMAGETAG}", $imageTag, $apiURL);
                $imageUrls[] = $imageUrl;
            }


            $productEntity->setImage($imageUrls);
        }

        return $productEntities;
    }

}
