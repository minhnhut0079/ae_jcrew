<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductEntity
 *
 * @author user
 */
class ProductEntity {

//    public $id;
    public $pcurl;
    public $name;
    public $product_code;
    public $brand;
    public $color;
    public $color_code;
    public $sex;
    public $category1;
    public $category2;
    public $category3;
    public $category4;
    public $category5;
    public $category6;
    public $category7;
    public $category8;
    public $price;
    public $salesprice;
    public $Product_Detail1;
    public $Product_Detail2;
    public $Product_Detail3;
    public $Product_Detail4;
    public $Product_Detail5;
    public $Product_Detail6;
    public $Product_Detail7;
    public $Product_Detail8;
    public $Product_Detail9;
    public $Product_Detail0;
    public $size1;
    public $size2;
    public $size3;
    public $size4;
    public $size5;
    public $size6;
    public $size7;
    public $size8;
    public $size9;
    public $size10;
    public $size11;
    public $size12;
    public $size13;
    public $size14;
    public $size15;
    public $size16;
    public $size17;
    public $size18;
    public $size19;
    public $size20;
    public $image1;
    public $image2;
    public $image3;
    public $image4;
    public $image5;
    public $image6;
    public $image7;
    public $image8;
    public $image9;
    public $image10;
    public $stock;
    public $created_at;



    public function setProductDetail($param) {
        
        if (empty($param)) {
            return "";
        }
        $i = 1;
        foreach ($param as $key => $value) {
            $size = "Product_Detail" . $i;

            $value = htmlspecialchars($value);
            $value = str_replace("&amp;nbsp;", "\x20", $value);
            $value = htmlspecialchars_decode($value);
            $value = trim($value);

            $this->$size = $value;
            $i++;
        }
    }
    public function setSizeFitDetail($param) {
        
        if (empty($param)) {
            return "";
        }
        $i = 1;
        foreach ($param as $key => $value) {
            $size = "Size_Fit_Detail" . $i;

            $value = htmlspecialchars($value);
            $value = str_replace("&amp;nbsp;", "\x20", $value);
            $value = htmlspecialchars_decode($value);
            $value = trim($value);

            $this->$size =  $value;
            $i++;
        }
    }
    
    public function setCategory($param) {
        if (empty($param)) {
            return "";
        }
        $i = 1;
        foreach ($param as $key => $value) {
            $size = "category" . $i;
            $value = htmlspecialchars($value);
            $value = str_replace("&amp;nbsp;", "\x20", $value);
            $value = htmlspecialchars_decode($value);
            $value = trim($value);

            $this->$size = $value;
            $i++;
        }
    }

    public function setSizeFit($param) {
        if (empty($param)) {
            return "";
        }
        $i = 1;

        foreach ($param as $key => $value) {
            $size = "Size_Fit" . $i;
            $value = htmlspecialchars($value);
            $value = str_replace("&amp;nbsp;", "\x20", $value);
            $value = htmlspecialchars_decode($value);
            $value = trim($value);
            $this->$size = " " . $value;
            $i++;
        }
    }

    public function setAboutUrban($param) {
        if (empty($param)) {
            return "";
        }

        $i = 1;
        foreach ($param as $key => $value) {
            $size = "About_Urban_Renewal_Recycled_" . $i;
            $value = htmlspecialchars($value);
            $value = str_replace("&amp;nbsp;", "\x20", $value);
            $value = htmlspecialchars_decode($value);
            $value = trim($value);
            $this->$size = " " . $value;
            $i++;
        }
    }

    public function setDSize($param) {
        if (empty($param)) {
            return "";
        }
        $i = 1;
        
        foreach ($param as $key => $value) {
            $size = "D_size" . $i;
            $value = htmlspecialchars($value);
            $value = str_replace("&amp;nbsp;", "\x20", $value);
            $value = htmlspecialchars_decode($value);
            $value = trim($value);
            $this->$size = $value;
            $i++;
        }
    }

    public function setSize($param) {
        if (empty($param)) {
            return "";
        }
        $i = 1;
        foreach ($param as $key => $value) {
            $size = "size" . $i;
            $value = htmlspecialchars($value);
            $value = str_replace("&amp;nbsp;", "\x20", $value);
            $value = htmlspecialchars_decode($value);
            $value = trim($value);
            $this->$size = $value;
            $i++;
        }
    }

    public function setImage($param) {
        if (empty($param)) {
            return "";
        }
        $i = 1;
        foreach ($param as $key => $value) {
            $size = "image" . $i;
            $value = htmlspecialchars($value);
            $value = str_replace("&amp;nbsp;", "\x20", $value);
            $value = htmlspecialchars_decode($value);
            $value = trim($value);
            $this->$size = $value;
            $i++;
        }
    }

}
