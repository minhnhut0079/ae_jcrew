<?php

require_once 'vendor/autoload.php';
require_once 'TblSubCategoryUrls.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use JonnyW\PhantomJs\Client;

class Utility {

    public $useragent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 Safari/537.36";
    public $logger;

    function __construct() {
        $this->logger = Logger::getLogger('logAppender');
    }

    static function getEnv() {
        $hostname = php_uname("n");

        $env = "";

        if (strpos($hostname, "local")) {
            $env = "DEV";
        } elseif (strpos($hostname, "ip-") !== false) {
            $env = "PRD";
        } else {
            $env = "DEV";
        }

        return $env;
    }

    function getHtml($url) {

        $this->logger->info('Start Get HTML');
        $this->logger->info($url);

//        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_URL, $url);
//        curl_setopt($ch,CURLOPT_FOLLOWLOCATION,true);
//        curl_setopt($ch,CURLOPT_MAXREDIRS,3);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($ch,CURLOPT_AUTOREFERER,true);
//        curl_setopt($ch, CURLOPT_USERAGENT, $this->useragent);


//        $contents = curl_exec($ch);
//        curl_close($ch);
//        $file_content = 'http://www.ae.com'.str_replace('http://www.ae.com','',$url);
        $contents = file_get_contents($url);

        if (empty($contents)) {

            $this->logger->warn('Cannot Access Retry after 3 second' . date("Y/m/d/ H:i:s"));
            sleep(3);
            $this->logger->warn('Retry！' . date("Y/m/d/ H:i:s"));

            $this->logger->warn($url);
            $this->getHtml($url);
        }


        return $contents;
    }

}
