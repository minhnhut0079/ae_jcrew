<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TblProductUrl
 *
 * @author user
 */
class TblCategoryUrl {

    public function insert($pdo, $parentCategory = "", $category1= "", $category2= "",$category3= "", $categoryUrl= "") {
            $categoryUrl = 'http://www.ae.com'.str_replace('https://www.ae.com/','',$categoryUrl);
            $stmt = $pdo->prepare("INSERT INTO `tbl_category_urls` ( "
                    . "`parent_category`,"
                    . "`category1`, "
                    . "`category2`, "
                    . "`category3`, "
                    . "`category_url`, "
                    . "`created_at`) "
                    . "VALUES ( "
                    . "?, "
                    . "?, "
                    . "?, "
                    . "? ,"
                    . "? ,"
                    . "now()"
                    . ")");

            $stmt->bindParam(1, $parentCategory, PDO::PARAM_STR);
            $stmt->bindParam(2, $category1, PDO::PARAM_STR);
            $stmt->bindParam(3, $category2, PDO::PARAM_STR);
            $stmt->bindParam(4, $category3, PDO::PARAM_STR);
            $stmt->bindParam(5, $categoryUrl, PDO::PARAM_STR);
            $stmt->execute();
    }

    public function get($pdo) {
        $sql = 'SELECT * FROM tbl_category_urls WHERE status="TODO"';

        $stmt = $pdo->query($sql);

        $result = array();

        foreach ($stmt as $row) {
            $result[] = $row;
        }

        return $result;
    }

    public function update($pdo, $id, $status) {
        $sql = 'UPDATE tbl_category_urls SET status = :status WHERE id = :id';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(':status', $status, PDO::PARAM_STR);
        $stmt->bindValue(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
    }

    public function truncate($pdo) {
        $sql = 'TRUNCATE TABLE tbl_category_urls';
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
    }
}
