<?php
require_once 'Utility.php';
require_once 'config.php';


class Database {

    public $logger = "";
    public $env = "";

    function __construct($log) {
        $this->logger = $log;
        $this->env = Utility::getEnv();
    }

    function DB_connect() {
        global $config;
        
        $host = $config[$this->env]['db_host'];
        $dbname = $config[$this->env]['db_name'];
        $dbuser = $config[$this->env]['db_user'];
        $dbpass = $config[$this->env]['db_password'];

        try {
            $pdo = new PDO('mysql:host=' . $host . ';dbname=' . $dbname . ';charset=utf8', $dbuser, $dbpass, array(PDO::ATTR_EMULATE_PREPARES => false));
            return $pdo;
        } catch (PDOException $e) {
            exit('データベース接続失敗。' . $e->getMessage());
        }
    }

    function DB_disconnect($pdo) {
        $pdo = null;
    }

}
