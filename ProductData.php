<?php

require_once 'phpQuery-onefile.php';
require_once 'Utility.php';
require_once 'ProductParser.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductData
 *
 * @author user
 */
class ProductData {

    public $logger = "";

    function __construct($log) {
        $this->logger = $log;
    }

    public function scrape($productUrl, $retryFlg = false) {

        $utility = new Utility();
        $productEntity = new ProductEntity();
        $productParser = new ProductParser();
        $sizeUrls = $this->checkDifferentSize($productUrl);
        $productEntityList = array();

        $html = $utility->getHtml($productUrl);

        $productEntities = $productParser->parse($html, $productUrl);

        $productEntityList[] = $productEntities;
        
        //複数サイズが合った場合更に取得
        $this->logger->info("複数サイズチェック開始");
        if (!empty($sizeUrls)) {

             $this->logger->info("複数サイズチェック発見");

            foreach ($sizeUrls as $sizeUrl) {
                
                if(empty($sizeUrl)){
                     $this->logger->info("空URLなのでスキップ");
                    continue;
                }
                
                $this->logger->info($sizeUrl);

                $html = $utility->getHtml($sizeUrl);
                $productEntities = $productParser->parse($html, $sizeUrl);
                
                $productEntityList[] = $productEntities;
            }
        }
        $this->logger->info("複数サイズチェック終了");

        return $productEntityList;
    }

    public function checkDifferentSize($productURL) {

        $utility = new Utility();

        // APIをコールする
        $baseAPIURL = "https://www.jcrew.com/browse2/ajax/product_details_ajax.jsp?sRequestedURL={URL}&prodCode={CODE}";

        $url = urlencode($productURL);

        preg_match("@.*/(.*?).jsp@i", $productURL, $matches);
        $productCode = $matches[1];
        $apiURL = str_replace("{URL}", $url, $baseAPIURL);
        $apiURL = str_replace("{CODE}", $productCode, $apiURL);

        $html = $utility->getHtml($apiURL);

        //petit,tallなどのサイズ違いがあるかチェックする。
        $html = "<HTML><HEAD></HEAD><BODY>" . $html . "</BODY></HTML>";
        $doc = phpQuery::newDocument($html);

        $diffUrlList = $doc['input:not(:checked)'];

        preg_match_all('@data-varianturl="(.*?)"@i', $diffUrlList, $matches);

        // 別サイズのURL
        $sizeUrls = array();
        foreach ($matches[1] as $value) {
            $sizeUrls[] = $value;
        }

        return $sizeUrls;
    }
    public function getProductDetail($pdo,$productUrl,$parent_category){
        $utility = new utility();
        $html = $utility->getHtml($productUrl);
        $doc = phpQuery::newDocument($html);
        $breadcrumbs = $doc['div.breadcrumbs'];
        $patter = '@href="(.*?)".*?>(.*?)</@i';
        preg_match_all($patter, $breadcrumbs, $matches);
        $breadcrumbs = array_combine($matches[2], $matches[1]);
        unset($breadcrumbs['Home']);


        $ProductParser = new ProductParser();
        $product = array();
        $exp = explode('product_details.jsp?',$productUrl);
        $json_hreft = 'https://www.ae.com/web/browse/productJSON.jsp?'.$exp[1];
        $json_file = file_get_contents($json_hreft);
        $json = json_decode($json_file);

        $TblExportProductDatas = new TblExportProductDatas();

        foreach($json->availableColorIds as $key => $value){

            $detail = $json->colorImageSelectionData->$value;
            $product[$key]['pcurl'] = $productUrl;
            $product[$key]['name'] = $detail->prdName;
            $product[$key]['product_code'] = $detail->colorPrdId;
            $product[$key]['brand'] = $json->brandName;
            $product[$key]['color'] = $detail->colorName;
            $product[$key]['color_code'] = $detail->colorId;
            $product[$key]['sex'] = $parent_category;

            $t = 1;
            foreach($breadcrumbs as $br_key => $br_value){
                $product[$key]['category'.$t] = $br_key;
                $t++;
            }
            $product[$key]['price'] = $json->convertedListPrice;
            $product[$key]['salesprice'] = $json->convertedSalePrice;
            $product[$key]['Product_Detail1'] = $json->leadingEquity;
            $product[$key]['Product_Detail2'] = 'Style: '.$json->classId.'-'.$json->styleId.' | Color: '.$json->availableColorIds[0].'';
            $product[$key]['Product_Detail3'] = $json->equityBullets[0];
            $product[$key]['Product_Detail4'] = $json->equityBullets[1];
            $product[$key]['Product_Detail5'] = $json->equityBullets[2];
            $product[$key]['Product_Detail6'] = $json->equityBullets[3];
            $product[$key]['Product_Detail7'] = $json->equityBullets[4];
            $product[$key]['Product_Detail8'] = $json->fabricContent;
            $product[$key]['Product_Detail9'] = $json->careInstructions;

            $i = 1;
            foreach($detail->selectionInfo as $_key => $_value){
               $product[$key]['size'.$i] = $_key;
               $stock = 0;
               foreach($_value as $dt_value){
                   $stock = $stock + ((int)$dt_value->stock);
               }
               if($stock == 0){
                   $product[$key]['stock'] = '◯';
               }else{
                   $product[$key]['stock']='';
               }
                $i++;

                // get image in table
                $common_url = 'http://www.ae.com/web/browse/product_details.jsp?productId='.$detail->colorPrdId;
                $image = $ProductParser->getImage($common_url);
                if( !empty($image)){
                    $v = 0;
                    foreach($image as $_img){
                        $product[$key]['image'.$v] = 'http://pics.ae.com/is/image/aeo/'.$_img.'?fit=stretch&hei=620&qlt=84,0&fit=stretch&hei=620&qlt=84,0&id=9aQp_2&wid=930&hei=620&fmt=jpg';
                        $v++;
                    }
                }
            }

        }

        foreach($product as $_product){
            $TblExportProductDatas->insert($_product,$pdo);
        }

        return $product;


    }

}
