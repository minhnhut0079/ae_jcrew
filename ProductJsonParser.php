<?php

require_once 'ProductEntity.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductJsonParser
 *
 * @author user
 */
class ProductJsonParser {

    public function parse($json, $categorytJson, $id, $category, $url) {
        $productArray = json_decode($json, true);
        $categorytArray = json_decode($categorytJson, true);

        $currentColor = "";
        $productEntityList = array();
        $sizeArray = array();
        $imageUrlArray = array();
        $cateArray = array();
        $productEntity = "";
        $i = 1;
        $baseImageUrl = 'http://images.urbanoutfitters.com/is/image/UrbanOutfitters/{productID}_{alphabet}?$xlarge$&defaultImage=';

        //detailだけ下準備
        $longDescription = strip_tags($productArray['product']['longDescription'], '<br>');


        foreach ($categorytArray["category"]["ancestors"] as $value) {
            $cateArray[] = $value["displayName"];
        }

        $cateArray[] = $categorytArray["category"]["displayName"];

        $sex = $this->detectSex($url);

        $flg = "";
        $contentCare = array();
        $sizeFit = array();
        $Dsize = array();
        $aboutUrbanRenewalRecycled = array();

        $value = explode("<br />", $longDescription);
        $value = array_filter($value);

        $description = $value[0];

        foreach ($value as $v) {

            $string = preg_replace("/( |　)/", "", $v);

            if (strpos($string, "Content+Care") !== FALSE) {
                $flg = "Care";
                continue;
            }

            if (strpos($string, "Size+Fit") !== FALSE) {
                $flg = "SizeFit";
                continue;
            }

            if (strpos($string, "Size") !== FALSE) {
                $flg = "Size";
                continue;
            }

            if (strpos($string, "AboutUrbanRenewalRecycled") !== FALSE) {
                $flg = "About";
                continue;
            }

            if ($flg == "Care") {
                $contentCare[] = $v;
            }

            if ($flg == "SizeFit") {
                $sizeFit[] = $v;
            }

            if ($flg == "About") {
                $aboutUrbanRenewalRecycled[] = $v;
            }

            if ($flg == "Size") {
                $Dsize[] = $v;
            }
        }

        $brand = $productArray["product"]["brand"];

        foreach ($productArray["product"]["skusInfo"] as $productVariety) {


            if ($productVariety["color"] != $currentColor && $productVariety["stockLevel"] != 0) {
                $currentColor = $productVariety["color"];

                $i = 1;
                if (!empty($productEntity)) {
                    $productEntityList[] = $productEntity;
                }
                $productEntity = new ProductEntity;

                $productEntity->pcurl = "http://www.urbanoutfitters.com/urban/catalog/productdetail.jsp?id=" . $productVariety["productIds"][0] . "&color=" . $productVariety["colorId"];
                $productEntity->color = $currentColor;
                $productEntity->sex = $sex;


                $productEntity->description = $description;
                $productEntity->name = $productArray["product"]["displayName"];
                $productEntity->brand = $brand;
                $productEntity->productcode = "uo-" . $productVariety["productIds"][0] . "-" . $productVariety["colorId"];

                //金額を入れる
                if ($productVariety["priceLists"][0]["listPrice"] != $productVariety["priceLists"][0]["salePrice"]) {
                    $productEntity->saleprice = $productVariety["priceLists"][0]["salePrice"];
                } elseif (!empty($productArray["product"]["customText1Value"])) {
                    $productEntity->saleprice = $productArray["product"]["customText1Value"];
                }

                $productEntity->price = $productVariety["priceLists"][0]["listPrice"];


                //画像URLを構築する。
                foreach ($productArray["product"]["colors"] as $colors) {

                    if ($colors["displayName"] == $currentColor) {
                        foreach ($colors["viewCode"] as $alphabet) {

                            $url = str_replace("{productID}", $colors["id"], $baseImageUrl);
                            $url = str_replace("{alphabet}", $alphabet, $url);

                            $imageUrlArray[] = $url;
                        }
                    }
                }
                $productEntity->setCategory($cateArray);
                $productEntity->setImage($imageUrlArray);
                $productEntity->setContentCare($contentCare);
                $productEntity->setSizeFit($sizeFit);
                $productEntity->setAboutUrban($aboutUrbanRenewalRecycled);
                $productEntity->setDSize($Dsize);
            }

            // サイズを入れる。在庫数があるものだけをサイズとしてカウント
            if ($productVariety["stockLevel"] != 0) {
                $size = "size" . $i;
                $productEntity->$size = $productVariety["size"];
                $i++;
            }
        }
        $productEntityList[] = $productEntity;


        return $productEntityList;
    }

    public function detectSex($url) {

        // MAN:SALE_M,M_ONLINE,UOWW-M,M_,MENS,M-
        // WOMEN:W-,SALE_W,W_,WOMENS
        
        $manIdentifer = array("SALE_M","M_ONLINE","UOWW-M","M_","MENS","M-");
        $womenidentifer = array("W-","SALE_W","W_,WOMENS","UOWW-M","W_ONLINE");
        
        if(in_array($url, $manIdentifer)){
            return "MEN";
        }
        
        if(in_array($url, $womenidentifer)){
            return "WOMEN";
        }
        
        return $url;
        
    }

}
