<?php

require_once 'vendor/autoload.php';

/**
 * Description of DuplicateUtility
 *
 * @author user
 */
class DuplicateUtility {

    public $logger;

    function __construct() {
        $this->logger = Logger::getLogger('logAppender');
    }

    public function getDeleteUrls($sameProductUrls) {

        $leaveUrl = "";
        $deleteUrls = array();
        foreach ($sameProductUrls as $productUrl) {

            //まず、1個目をセット
            if ($leaveUrl == "") {
                $leaveUrl = $productUrl;
                continue;
            }

            //2個目以降ならカテゴリ階層を比較
            $category = "category";

            $this->logger->info('カレントURL-->' . $leaveUrl['pcurl']);
            $leave = $this->checkCategoryLevel($leaveUrl);
            $leaveCategory = $category . $leave;
            $this->logger->info('カテゴリレベル' . $leave . '   カテゴリ名' . $leaveUrl[$leaveCategory]);


            $this->logger->info('比較URL-->' . $productUrl['pcurl']);
            $new = $this->checkCategoryLevel($productUrl);
            $newCategory = $category . $new;
            $this->logger->info('カテゴリレベル' . $new . '    カテゴリ名' . $productUrl[$newCategory]);

            
            if ($new > $leave) {
                $deleteUrls[] = $leaveUrl;
                $leaveUrl = $productUrl;
            } else {
                $deleteUrls[] = $productUrl;
            }
        }
        return $deleteUrls;
    }

    public function checkCategoryLevel($targetUrl) {

        if (!empty($targetUrl['category4'])) {
            return 4;
        }
        if (!empty($targetUrl['category3'])) {
            return 3;
        }
        if (!empty($targetUrl['category2'])) {
            return 2;
        }
        if (!empty($targetUrl['category1'])) {
            return 1;
        }
    }

    //put your code here
}
